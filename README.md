# Twittalyst - Twitter Analysing System 

## External Dependencies

* The project was created with JDK 7

* The project is managed with Maven 3.0

* The project requires a running mongo-db instance

## How to build

Go to the project directory and type `mvn clean install`


## Components

### twittalyst-mine 

Mines a set of connected tweets from the Twitterstream and dumps them into a file.

### twittalyst-analyzer

Analyzes an existing Tweet-Dump and stores the data in the Relational DB and the Graph DB.

### How to start the application

Execute the command

`mvn exec:java -pl twittalyst-analyzer -Dexec.args="<pathToDump>"`

### twittalyst-datastore

The NoSQL infrastructure containing

* RDBMS for storing user and user-metadata
* Document database for storing ads and ad-campaigns
* Graph database for storing interactions between Twitter users

### twittalyst-webui

Web UI frontend for querying the data and managing ads


## Setup

### Aqquire mongo-db

Go to [http://www.mongodb.org/downloads](http://www.mongodb.org/downloads) and download Version 2.0.4 or above.

Install and execute according to instructions on [http://www.mongodb.org/](http://www.mongodb.org/)

#### Start the server

Start the database with the following command

* For windows: `mongod.exe --dbpath <db-path>`
* For linux: `mongod --dbpath <db-path>`

#### Import initial Topics

Import the Topics collection in to your mongodb:    
in `./etc/topicsdb` do:     
`mongorestore -db aic --dir aic`

### Start the web application

`mvn jetty:run -pl twittalyst-webui`

and visit http://localhost:8080
