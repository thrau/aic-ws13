Twittalyst - Web UI
===================

Running
-------

### With Maven

Execute

    cd twittalyst-webui
    mvn jetty:run


and visit `http://localhost:8080/`


### From Eclipse

Get the [Run-Jetty-Run](https://code.google.com/p/run-jetty-run/) Eclipse plugin and run the application as described.

Then visit `http://localhost:8080/twittalyst-webui/`
