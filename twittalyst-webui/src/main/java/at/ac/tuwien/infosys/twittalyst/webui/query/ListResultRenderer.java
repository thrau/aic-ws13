package at.ac.tuwien.infosys.twittalyst.webui.query;

import org.apache.wicket.Component;

/**
 * ListResultRenderer
 */
public interface ListResultRenderer<T> extends QueryResultRenderer<ListQueryResult<T>> {

    @Override
    Component render(String id, QueryParameters parameters, ListQueryResult<T> result);
}
