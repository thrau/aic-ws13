package at.ac.tuwien.infosys.twittalyst.webui.components;

import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
* ValueColumn
*/
public class ValueColumn<T, S> extends AbstractColumn<T, S> {

    ColumnValueCallback<T, S> callback;

    public ValueColumn(String displayModel, ColumnValueCallback<T, S> callback) {
        this(Model.of(displayModel), callback);
    }

    public ValueColumn(IModel<String> displayModel, ColumnValueCallback<T, S> callback) {
        super(displayModel);
        this.callback = callback;
    }

    @Override
    public void populateItem(Item<ICellPopulator<T>> item, String id, IModel<T> row) {
        S call = callback.call(row);
        item.add(new Label(id, call.toString()));
    }
}
