package at.ac.tuwien.infosys.twittalyst.webui.service;

public interface ILabelGenerator {
    String generateLabel();
}
