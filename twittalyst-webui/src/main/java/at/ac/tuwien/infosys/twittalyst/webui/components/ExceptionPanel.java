package at.ac.tuwien.infosys.twittalyst.webui.components;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;

/**
 * ExceptionPanel
 */
public class ExceptionPanel extends AlertPanel {

    public ExceptionPanel(String id, Throwable cause) {
        this(id, cause.getLocalizedMessage(), cause);

    }

    public ExceptionPanel(String id, String text, Throwable cause) {
        super(id, text, AlertType.DANGER);
        setDefaultModel(Model.of(cause));
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new Label("stacktrace", getModelObjectAsString()));
    }

    private String getModelObjectAsString() {
        return getStackTrace((Throwable) getDefaultModelObject());
    }

    private String getStackTrace(Throwable cause) {
        StringWriter str = new StringWriter();

        try (PrintWriter w = new PrintWriter(str)) {
            cause.printStackTrace(w);
            w.flush();
        }

        return str.toString();
    }

}
