package at.ac.tuwien.infosys.twittalyst.webui.components;

import java.util.ArrayList;
import java.util.List;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.InterestedRelation;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.DataGridView;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.PropertyPopulator;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.TopicMention;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.UserInteraction;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;

/**
 * UserResultPanel
 */
public class UserResultPanel extends Panel {

    private static final Logger LOG = LoggerFactory.getLogger(UserResultPanel.class);

    public UserResultPanel(String id, User object) {
        super(id, new Model<>(object));
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        addPropertyLabel(getDefaultModel(), "id", "name", "tweetCount", "followerCount", "friendCount");
    }

    public void addPropertyLabel(String property) {
        add(new Label(property, new PropertyModel<>(getDefaultModel(), property)));
    }

    public void addPropertyLabel(String... properties) {
        for (String property : properties) {
            addPropertyLabel(property);
        }
    }

    public void addPropertyLabel(IModel<?> model, String property) {
        add(new Label(property, new PropertyModel<>(model, property)));
    }

    public void addPropertyLabel(IModel<?> model, String... properties) {
        for (String property : properties) {
            addPropertyLabel(model, property);
        }
    }

    public abstract static class UserInteractionPanel extends ActiveQueryPanel {

        public List<ICellPopulator<UserInteraction>> columns;

        public List<UserInteraction> interactions;

        public UserInteractionPanel() {
            super("userInteractions", "Interactions");

            columns = new ArrayList<>(2);
            columns.add(new PropertyPopulator<UserInteraction>("user.name"));
            columns.add(new PropertyPopulator<UserInteraction>("interactions"));

            interactions = new ArrayList<>();
        }

        @Override
        protected void onInitialize() {
            super.onInitialize();

            add(new DataGridView<>("rows", columns, new ListDataProvider<>(interactions)));
        }

        protected void setData(List<UserInteraction> interactions) {
            this.interactions.clear();
            this.interactions.addAll(interactions);
        }

    }

    public abstract static class UserTopicsPanel extends ActiveQueryPanel {

        public List<ICellPopulator<InterestedRelation>> columns;

        public List<InterestedRelation> topics;

        protected UserTopicsPanel(String id, String title) {
            super(id, title);
        }

        public UserTopicsPanel() {
            this("userTopics", "Interests");

            columns = new ArrayList<>(2);
            columns.add(new PropertyPopulator<InterestedRelation>("topic.topicName"));
            columns.add(new PropertyPopulator<InterestedRelation>("weight"));

            topics = new ArrayList<>();
        }

        @Override
        protected void onInitialize() {
            super.onInitialize();

            add(new DataGridView<>("rows", columns, new ListDataProvider<>(topics)));
        }

        protected void setData(List<InterestedRelation> topics) {
            this.topics.clear();
            this.topics.addAll(topics);
        }

    }

    public abstract static class UserSuggestedTopicsPanel extends ActiveQueryPanel {

        public List<ICellPopulator<String>> columns;

        public List<String> topics;

        protected UserSuggestedTopicsPanel() {
            super("userSuggestedTopics", "Suggested topics");

            columns = new ArrayList<>(1);
            columns.add(new ICellPopulator<String>() {
                @Override
                public void populateItem(Item<ICellPopulator<String>> item, String id, IModel<String> rowModel) {
                    item.add(new Label(id, rowModel));
                }

                @Override
                public void detach() {
                    // pass
                }
            });

            topics = new ArrayList<>();
        }

        @Override
        protected void onInitialize() {
            super.onInitialize();

            add(new DataGridView<>("rows", columns, new ListDataProvider<>(topics)));
        }

        protected void setData(List<String> topics) {
            this.topics.clear();
            this.topics.addAll(topics);
        }
    }
}
