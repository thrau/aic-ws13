package at.ac.tuwien.infosys.twittalyst.webui.query;

import java.io.Serializable;

import org.apache.wicket.Component;

/**
 * QueryResultRenderer
 */
public interface QueryResultRenderer<T extends QueryResult> extends Serializable {

    /**
     * Renders the given QueryResult into a Wicket Component
     * 
     * @param id the components id
     * @param parameters the parameters the query was execute with
     * @param result the query result to render
     * @return a Component representing the query result
     */
    Component render(String id, QueryParameters parameters, T result);
}
