package at.ac.tuwien.infosys.twittalyst.webui.service;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class LabelGenerator implements ILabelGenerator {

    private static final String[] LABELS = {
        "It works!",
        "this is how we generate our shit.",
        "It compiles! Ship it!",
        "I expected something different.",
        "Become a programmer, they said. It'll be fun, they said."
    };

    private static final Random RNG = new Random(System.currentTimeMillis());

    @Override
    public String generateLabel() {
        return LABELS[RNG.nextInt(LABELS.length)];
    }

}
