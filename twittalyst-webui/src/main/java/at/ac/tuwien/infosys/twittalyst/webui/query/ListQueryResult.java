package at.ac.tuwien.infosys.twittalyst.webui.query;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * ListQueryResult
 */
public class ListQueryResult<T> extends SimpleQueryResult<List<T>> implements QueryResult<List<T>>, Iterable<T> {
    public ListQueryResult(List<T> values) {
        super(values);
    }

    public ListQueryResult(T[] values) {
        super(Arrays.asList(values));
    }

    @Override
    public Iterator<T> iterator() {
        return getValue().iterator();
    }

    @Override
    public boolean isEmpty() {
        return getValue().isEmpty();
    }
}
