package at.ac.tuwien.infosys.twittalyst.webui;

import javax.inject.Inject;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.webui.components.LoadingIndicator;
import at.ac.tuwien.infosys.twittalyst.webui.components.QueryForm;
import at.ac.tuwien.infosys.twittalyst.webui.query.QueryParameters;
import at.ac.tuwien.infosys.twittalyst.webui.query.provider.UserByNameQueryProvider;

/**
 * UserQueryPage
 */
public class UserQueryPage extends BasePage {

    private static final Logger LOG = LoggerFactory.getLogger(UserQueryPage.class);

    @Inject
    UserByNameQueryProvider queryProvider;

    WebMarkupContainer userPanelContainer;

    private static final String userPanelId = "userPanel";

    public UserQueryPage() {
    }

    public UserQueryPage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new UserQueryForm("queryForm"));

        add(userPanelContainer = new WebMarkupContainer("userPanelContainer"));
        userPanelContainer.setOutputMarkupId(true);

        userPanelContainer.add(new WebMarkupContainer(userPanelId));
    }

    public class UserQueryForm extends QueryForm {

        public UserQueryForm(String id) {
            super(id);
        }

        @Override
        protected void populateForm(Form<QueryParameters> form) {
            form.add(new TextField<>("name").setLabel(Model.of("Name")).setType(String.class));
        }

        @Override
        protected void onSubmit(AjaxRequestTarget target, Form<QueryParameters> form) {
            QueryParameters params = form.getModelObject();

            Component render =
                queryProvider.getResultRenderer().render(userPanelId, params, queryProvider.getQuery().run(params));

            userPanelContainer.replace(render);
            target.add(userPanelContainer);
        }

        @Override
        protected void onClick(AjaxRequestTarget target) {
            userPanelContainer.replace(new LoadingIndicator(userPanelId));
            target.add(userPanelContainer);
        }

    }

}
