package at.ac.tuwien.infosys.twittalyst.webui.components;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration;

/**
 * ActiveQueryPanel
 */
public abstract class ActiveQueryPanel extends Panel {

    private IModel<?> title;

    private boolean loading;
    private boolean started;

    protected WebMarkupContainer loadingContainer;
    protected WebMarkupContainer contentContainer;

    public ActiveQueryPanel(String id) {
        super(id);
    }

    public ActiveQueryPanel(String id, IModel<?> title) {
        super(id);
        this.title = title;
    }

    public ActiveQueryPanel(String id, IModel<?> title, IModel<?> model) {
        super(id, model);
        this.title = title;
    }

    public ActiveQueryPanel(String id, String title) {
        this(id, Model.of(title));
    }

    public ActiveQueryPanel(String id, String title, IModel<?> model) {
        this(id, Model.of(title), model);
    }

    private IModel<?> getTitle() {
        return title;
    }

    private ActiveQueryPanel setTitle(String title) {
        setDefaultModelObject(title);
        return this;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        super.add(new Label("title", getTitle()));

        super.add(loadingContainer = new WebMarkupContainer("loading-container") {
            @Override
            public boolean isVisible() {
                return isLoading() || !isStarted();
            }
        });

        super.add(contentContainer = new WebMarkupContainer("content-container"));

        super.add(new AbstractAjaxTimerBehavior(Duration.seconds(1)) {
            @Override
            protected void onTimer(AjaxRequestTarget target) {
                if (!isLoading()) {
                    contentContainer.setVisible(true);
                    target.add(getComponent());
                    onAfterLoad(target);
                    stop(target);
                }
            }
        });

        setOutputMarkupId(true);
        loadingContainer.setOutputMarkupId(true);
        contentContainer.setOutputMarkupId(true);

        contentContainer.setVisible(false);
    }

    protected void onAfterLoad(AjaxRequestTarget target) {
        // hook
    }

    @Override
    protected void onAfterRender() {
        super.onAfterRender();

        dispatch();
    }

    protected void dispatch() {
        if (started) {
            return;
        }
        started = true;

        new Thread(new Runnable() {
            @Override
            public void run() {
                setLoading(true);
                try {
                    doCall();
                } finally {
                    setLoading(false);
                }
            }
        }).start();
    }

    protected abstract void doCall();

    @Override
    public ActiveQueryPanel add(Component... child) {
        contentContainer.add(child);
        return this;
    }

    public boolean isLoading() {
        return loading;
    }

    public boolean isStarted() {
        return started;
    }

    private void setLoading(boolean loading) {
        this.loading = loading;
    }

}
