package at.ac.tuwien.infosys.twittalyst.webui.components;

import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

import at.ac.tuwien.infosys.twittalyst.webui.query.QueryParameters;

/**
 * UserQueryForm
 */
public class QueryForm extends Panel {

    private Form<QueryParameters> form;

    public QueryForm(String id) {
        this(id, new QueryParameters());
    }

    @SuppressWarnings("unchecked")
    public QueryForm(String id, QueryParameters parameters) {
        super(id);
        setOutputMarkupId(true);

        add(createForm("form", new CompoundPropertyModel<>(parameters)));
    }

    private Form<QueryParameters> createForm(String id, IModel<QueryParameters> model) {
        form = new Form<>(id, model);
        form.setOutputMarkupId(true);

        form.add(new AjaxButton("submit", form) {
            @SuppressWarnings("unchecked")
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                QueryForm.this.onSubmit(target, (Form<QueryParameters>) form);

            }
        }.add(new AjaxEventBehavior("click") {
            @Override
            protected void onEvent(AjaxRequestTarget target) {
                QueryForm.this.onClick(target);
            }
        }));

        populateForm(form);

        return form;
    }

    public Form<QueryParameters> getForm() {
        return form;
    }

    protected void populateForm(Form<QueryParameters> form) {
        // hook
    }

    protected void onSubmit(AjaxRequestTarget target, Form<QueryParameters> form) {
        // hook
    }

    protected void onClick(AjaxRequestTarget target) {
        // hook
    }

}
