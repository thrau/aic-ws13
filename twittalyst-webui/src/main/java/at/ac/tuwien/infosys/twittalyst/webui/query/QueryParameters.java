package at.ac.tuwien.infosys.twittalyst.webui.query;

import java.util.Hashtable;

import org.apache.wicket.util.io.IClusterable;

/**
 * Hastable to hold possible parameters for a query.
 */
public class QueryParameters extends Hashtable<String, Object> implements IClusterable {

    public QueryParameters() {
        super();
    }

    public QueryParameters(int initialCapacity) {
        super(initialCapacity);
    }

    public <T> T get(Object key, Class<T> type, T defaultValue) {
        Object value = get(key);
        return (value == null) ? defaultValue : type.cast(value);
    }

    public <T> T get(Object key, Class<T> type) {
        return get(key, type, null);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(Object key, T defaultValue) {
        return get(key, (Class<T>) defaultValue.getClass(), defaultValue);
    }

    public Boolean getBool(Object key) {
        return get(key, Boolean.class);
    }

    public Integer getInt(Object key) {
        return get(key, Integer.class);
    }

    public String getString(Object key) {
        return get(key, String.class);
    }

    public Boolean getBool(Object key, Boolean defaultValue) {
        return get(key, Boolean.class, defaultValue);
    }

    public Integer getInt(Object key, Integer defaultValue) {
        return get(key, Integer.class, defaultValue);
    }

    public String getString(Object key, String defaultValue) {
        return get(key, String.class, defaultValue);
    }

}
