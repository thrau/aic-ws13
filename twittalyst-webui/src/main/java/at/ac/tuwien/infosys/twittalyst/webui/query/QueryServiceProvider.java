package at.ac.tuwien.infosys.twittalyst.webui.query;

import java.io.Serializable;

/**
 * QueryServiceProvider
 */
public interface QueryServiceProvider<T> extends Serializable {

    /**
     *
     * @return
     */
    RunnableQuery<T> getQuery();

    /**
     * 
     * @return
     */
    QueryResultRenderer<? extends QueryResult<T>> getResultRenderer();
}
