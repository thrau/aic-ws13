package at.ac.tuwien.infosys.twittalyst.webui.query;

/**
 * SimpleQueryResult
 */
public class SimpleQueryResult<T> implements QueryResult<T> {

    private T value;

    public SimpleQueryResult(T value) {
        this.value = value;
    }

    @Override
    public Class<?> getType() {
        return value.getClass();
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public boolean isEmpty() {
        return value == null;
    }
}
