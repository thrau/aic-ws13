package at.ac.tuwien.infosys.twittalyst.webui.query;

import java.util.List;

/**
 * ListQuery
 */
public interface ListQuery<T> extends RunnableQuery<List<T>> {
    @Override
    ListQueryResult<T> run(QueryParameters parameters);
}
