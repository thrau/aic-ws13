package at.ac.tuwien.infosys.twittalyst.webui.components;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * AlertPanel
 */
public class AlertPanel extends Panel {

    public static enum AlertType {
        SUCCESS,
        INFO,
        WARNING,
        DANGER
    }

    private AlertType type;

    public AlertPanel(String id, String text) {
        this(id, text, AlertType.INFO);
    }

    public AlertPanel(String id, String text, AlertType type) {
        this(id, Model.of(text), type);
    }

    public AlertPanel(String id, IModel<String> text, AlertType type) {
        super(id, text);
        this.type = type;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new AttributeAppender("class", getCssClass()));
        add(new Label("text", getDefaultModelObjectAsString()));
    }

    private String getCssClass() {
        return "alert alert-" + type.toString().toLowerCase();
    }
}
