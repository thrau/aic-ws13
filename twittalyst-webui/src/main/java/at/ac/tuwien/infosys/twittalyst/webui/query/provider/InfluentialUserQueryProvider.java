package at.ac.tuwien.infosys.twittalyst.webui.query.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.Component;
import org.apache.wicket.model.util.ListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.ac.tuwien.infosys.twittalyst.datastore.queries.BigDataQuery;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;
import at.ac.tuwien.infosys.twittalyst.webui.components.UserInfluencePanel;
import at.ac.tuwien.infosys.twittalyst.webui.model.UserInfluence;
import at.ac.tuwien.infosys.twittalyst.webui.query.ListQuery;
import at.ac.tuwien.infosys.twittalyst.webui.query.ListQueryResult;
import at.ac.tuwien.infosys.twittalyst.webui.query.ListResultRenderer;
import at.ac.tuwien.infosys.twittalyst.webui.query.QueryParameters;
import at.ac.tuwien.infosys.twittalyst.webui.query.QueryServiceProvider;

/**
 * InfluentialUserQueryProvider
 */
@Service
public class InfluentialUserQueryProvider implements QueryServiceProvider<List<UserInfluence>> {

    @Autowired
    BigDataQuery service;

    @Override
    public ListQuery<UserInfluence> getQuery() {
        return new ListQuery<UserInfluence>() {

            @Override
            public String getDescription() {
                return "Returns the most influential users in the dataset based on ...";
            }

            @Override
            public ListQueryResult<UserInfluence> run(QueryParameters parameters) {
                Integer limit = parameters.getInt("limit", 10);

                List<Map<String, Object>> result = service.getMostInfluentialUsers(limit);

                return new ListQueryResult<>(mapResult(result));
            }

            private List<UserInfluence> mapResult(List<Map<String, Object>> rows) {
                List<UserInfluence> ret = new ArrayList<>(rows.size());

                for (Map<String, Object> row : rows) {
                    ret.add(mapRow(row));
                }
                return ret;
            }

            private UserInfluence mapRow(Map<String, Object> row) {
                return new UserInfluence((User) row.get("user"), (Long) row.get("interactions"));
            }

        };
    }

    @Override
    public ListResultRenderer<UserInfluence> getResultRenderer() {
        return new ListResultRenderer<UserInfluence>() {
            @Override
            public Component render(String id, QueryParameters parameters, ListQueryResult<UserInfluence> result) {
                return new UserInfluencePanel(id, new ListModel<>(result.getValue()));
            }
        };
    }
}
