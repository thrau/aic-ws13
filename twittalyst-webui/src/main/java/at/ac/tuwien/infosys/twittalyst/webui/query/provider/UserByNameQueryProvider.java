package at.ac.tuwien.infosys.twittalyst.webui.query.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.InterestedRelation;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.UserInteraction;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.TopicRepository;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.UserRepository;
import at.ac.tuwien.infosys.twittalyst.datastore.queries.BigDataQuery;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.UserDAO;
import at.ac.tuwien.infosys.twittalyst.webui.components.AlertPanel;
import at.ac.tuwien.infosys.twittalyst.webui.components.UserResultPanel;
import at.ac.tuwien.infosys.twittalyst.webui.query.EmptyQueryResult;
import at.ac.tuwien.infosys.twittalyst.webui.query.QueryParameters;
import at.ac.tuwien.infosys.twittalyst.webui.query.QueryResult;
import at.ac.tuwien.infosys.twittalyst.webui.query.QueryResultRenderer;
import at.ac.tuwien.infosys.twittalyst.webui.query.QueryServiceProvider;
import at.ac.tuwien.infosys.twittalyst.webui.query.RunnableQuery;
import at.ac.tuwien.infosys.twittalyst.webui.query.SimpleQueryResult;

/**
 * UserByNameQueryProvider
 */
@Service
public class UserByNameQueryProvider implements QueryServiceProvider<User> {

    private static final Logger LOG = LoggerFactory.getLogger(UserByNameQueryProvider.class);

    @Autowired
    transient UserDAO dao;

    @Autowired
    transient UserRepository userRepository;

    @Autowired
    transient BigDataQuery bigDataQuery;

    @Autowired
    transient TopicRepository topicRepository;

    @Override
    public RunnableQuery<User> getQuery() {

        return new RunnableQuery<User>() {
            @Override
            public String getDescription() {
                return "Finds a user according to his name";
            }

            @Override
            public QueryResult<User> run(QueryParameters parameters) {
                if (parameters == null) {
                    throw new NullPointerException("Parameters are null");
                }

                String name = parameters.getString("name");

                if (name == null) {
                    return EmptyQueryResult.get();
                }

                User user = dao.getByName(name);

                if (user == null) {
                    return EmptyQueryResult.get();
                }

                return new SimpleQueryResult<>(user);
            }
        };
    }

    @Override
    public QueryResultRenderer<QueryResult<User>> getResultRenderer() {
        return new QueryResultRenderer<QueryResult<User>>() {
            @Override
            public Component render(String id, QueryParameters parameters, QueryResult<User> result) {
                final User value = result.getValue();

                WebMarkupContainer c;
                if (value != null) {
                    c = new UserResultPanel(id, value);
                    c.add(new UserResultPanel.UserInteractionPanel() {
                        @Override
                        protected void doCall() {
                            LOG.info("Retrieving interactions for user {}...", value.getName());

                            List<UserInteraction> interactions = bigDataQuery.outgoingUserInteractions(value.getID());

                            setData(interactions);
                            LOG.info("found {} interactions", interactions.size());
                        }
                    });
                    c.add(new UserResultPanel.UserTopicsPanel() {

                        @Override
                        protected void doCall() {
                            LOG.info("Retrieving mentioned topics for user {}...", value.getName());

                            Set<InterestedRelation> topics =
                                userRepository.findByUserId(value.getID()).getInterestedInTopics();

                            setData(new ArrayList<>(topics));
                            LOG.info("found {} mentions", topics.size());
                        }
                    });
                    c.add(new UserResultPanel.UserSuggestedTopicsPanel() {

                        @Override
                        protected void doCall() {
                            LOG.info("Retrieving suggested topics for user {}...", value.getName());

                            bigDataQuery.suggestTopicsForUser(value.getID());

                            List<String> topics =
                                bigDataQuery.suggestTopicsForUserBasedOnInteractions(value.getID(), 2);

                            setData(topics);
                            LOG.info("found {} suggestion", topics.size());
                        }
                    });

                } else {
                    String msg = "User " + parameters.getString("name") + " not found";
                    c = new AlertPanel(id, msg, AlertPanel.AlertType.WARNING);
                }

                return c;
            }
        };
    }
}
