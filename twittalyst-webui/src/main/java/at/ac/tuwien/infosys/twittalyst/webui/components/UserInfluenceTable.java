package at.ac.tuwien.infosys.twittalyst.webui.components;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.DataGridView;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.PropertyPopulator;
import org.apache.wicket.markup.html.panel.GenericPanel;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;

import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;
import at.ac.tuwien.infosys.twittalyst.webui.model.UserInfluence;

/**
 * UserInfluenceTable
 */
public class UserInfluenceTable extends GenericPanel<List<UserInfluence>> {

    private DataGridView<UserInfluence> gridView;

    public UserInfluenceTable(String id, IModel<List<UserInfluence>> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        initGridView();
    }

    public void updateGridView(AjaxRequestTarget target) {
        target.add(gridView);
    }

    private void initGridView() {
        List<ICellPopulator<UserInfluence>> columns = new ArrayList<>();

        populateColumns(columns);

        add(gridView = new DataGridView<>("rows", columns, new ListDataProvider<>(getModelObject())));
        gridView.setOutputMarkupId(true);
    }

    private void populateColumns(List<ICellPopulator<UserInfluence>> columns) {
        columns.add(new PropertyPopulator<UserInfluence>("user.name"));
        columns.add(new PropertyPopulator<UserInfluence>("user.followerCount"));
        columns.add(new PropertyPopulator<UserInfluence>("user.friendCount"));
        columns.add(new ValueColumn<>("ratio", new ColumnValueCallback<UserInfluence, String>() {
            @Override
            public String call(IModel<UserInfluence> model) {
                User u = model.getObject().getUser();
                double ratio = u.getFollowerCount() / (double) u.getFriendCount();

                return String.format("%.2f", ratio);
            }
        }));
        columns.add(new PropertyPopulator<UserInfluence>("user.tweetCount"));
        columns.add(new PropertyPopulator<UserInfluence>("interactions"));
    }

}
