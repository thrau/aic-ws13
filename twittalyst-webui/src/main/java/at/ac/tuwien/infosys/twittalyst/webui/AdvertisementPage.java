package at.ac.tuwien.infosys.twittalyst.webui;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.util.ListModel;
import org.apache.wicket.request.resource.DynamicImageResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.util.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.datastore.docdb.Advertisement;
import at.ac.tuwien.infosys.twittalyst.datastore.docdb.AdvertisementDAO;

/**
 * AdvertisementPage
 */
public class AdvertisementPage extends BasePage {

    @Inject
    AdvertisementDAO adsDao;

    @Override
    protected void onInitialize() {
        super.onInitialize();

        List<Advertisement> ads = adsDao.getAll();
        RepeatingView adsView = new RepeatingView("ads", new ListModel<>(ads));

        for (Advertisement advertisement : ads) {
            String id = adsView.newChildId();

            adsView.add(new AdvertisementFragment(id, advertisement));
        }

        add(adsView);
    }

    class AdvertisementFragment extends Fragment {
        AdvertisementFragment(String id, Advertisement ad) {
            super(id, "ad", AdvertisementPage.this, new Model<>(ad));

            add(new StreamedImage("image", ad.getPicture()));
            add(new Label("sponsor", PropertyModel.of(getDefaultModel(), "sponsor")));
            add(new Label("text", PropertyModel.of(getDefaultModelObject(), "adText")));
            add(new Label("topics", StringUtils.join(ad.getTopics(), ", ")));
            add(new Label("keywords", StringUtils.join(ad.getKeywords(), ", ")));
        }
    }

    public static class StreamedImage extends Image {

        private static final Logger LOG = LoggerFactory.getLogger(StreamedImage.class);

        private transient InputStream stream;

        public StreamedImage(String id, InputStream imageStream) {
            super(id);
            this.stream = imageStream;
        }

        @Override
        protected IResource getImageResource() {
            return new DynamicImageResource() {
                @Override
                protected byte[] getImageData(Attributes attributes) {
                    try {
                        return IOUtils.toByteArray(stream);
                    } catch (IOException e) {
                        LOG.error("IO Exception while reading input stream", e);
                        return new byte[0];
                    }
                }
            };
        }
    }
}
