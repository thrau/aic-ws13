package at.ac.tuwien.infosys.twittalyst.webui.components.navigation;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.Model;

/**
*
*/
public class NavigationLink<T> extends BookmarkablePageLink<T> {
    public <C extends Page> NavigationLink(String id, String label, Class<C> pageClass) {
        super(id, pageClass);
        setBody(Model.of(label));
    }

    public NavigationLink(String id, NavigationItem navigationItem) {
        this(id, navigationItem.getLabel(), navigationItem.getPageClass());
    }

    public boolean linksToCurrentPage() {
        return linksTo(getPage());
    }
}
