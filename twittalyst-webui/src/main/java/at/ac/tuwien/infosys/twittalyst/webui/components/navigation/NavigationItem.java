package at.ac.tuwien.infosys.twittalyst.webui.components.navigation;

import org.apache.wicket.Page;

/**
*
*/
public class NavigationItem {
    private String label;
    private Class<? extends Page> pageClass;

    public NavigationItem(String label, Class<? extends Page> pageClass) {
        this.label = label;
        this.pageClass = pageClass;
    }


    public String getLabel() {
        return label;
    }

    public Class<? extends Page> getPageClass() {
        return pageClass;
    }

}
