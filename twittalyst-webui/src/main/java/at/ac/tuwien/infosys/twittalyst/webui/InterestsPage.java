package at.ac.tuwien.infosys.twittalyst.webui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.apache.wicket.extensions.markup.html.repeater.data.grid.DataGridView;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.PropertyPopulator;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.TopicMention;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.UserTopicMentions;
import at.ac.tuwien.infosys.twittalyst.datastore.queries.BigDataQuery;
import at.ac.tuwien.infosys.twittalyst.webui.components.ActiveQueryPanel;
import at.ac.tuwien.infosys.twittalyst.webui.components.ColumnValueCallback;
import at.ac.tuwien.infosys.twittalyst.webui.components.ValueColumn;

/**
 * InterestsPage
 */
public class InterestsPage extends BasePage {

    private static final Logger LOG = LoggerFactory.getLogger(InterestsPage.class);

    @Inject
    BigDataQuery bigDataQuery;

    IModel<Integer> limit;

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new BroadRangeQueryPanel("broad", "Users with a broad range of interests"));
        add(new FocusedRangeQueryPanel("focused", "Users with a focused interest"));

        limit = Model.of(10);
    }

    public abstract class InterestRangeQueryPanel extends ActiveQueryPanel {

        protected List<UserTopicMentions> topicMentions;

        protected InterestRangeQueryPanel(String id, String title) {
            super(id, title);

            topicMentions = new ArrayList<>();
        }

        @Override
        protected void onInitialize() {
            super.onInitialize();

            List<ICellPopulator<UserTopicMentions>> columns = new ArrayList<>();

            columns.add(new PropertyPopulator<UserTopicMentions>("userName"));
            columns.add(new ValueColumn<>("Mentions", new ColumnValueCallback<UserTopicMentions, String>() {
                @Override
                public String call(IModel<UserTopicMentions> model) {
                    return join(model.getObject().getTopicMentionList());
                }
            }));

            add(new DataGridView<>("rows", columns, new ListDataProvider<>(topicMentions)));
        }

        protected String join(List<TopicMention> mentions) {
            StringBuffer str = new StringBuffer();

            Iterator<TopicMention> iterator = mentions.iterator();

            while (iterator.hasNext()) {
                TopicMention next = iterator.next();
                str.append(next.getTopicName());
                str.append(" (");
                str.append(next.getMentions());
                str.append(")");

                if (iterator.hasNext()) {
                    str.append(", ");
                }
            }

            return str.toString();
        }

        protected void setData(List<UserTopicMentions> mentions) {
            topicMentions.clear();
            topicMentions.addAll(mentions);
        }

    }

    private class BroadRangeQueryPanel extends InterestRangeQueryPanel {
        private BroadRangeQueryPanel(String id, String title) {
            super(id, title);
        }

        @Override
        protected void doCall() {
            long then = System.currentTimeMillis();
            LOG.info("Beginning calculation for broad interest range ...");
            setData(bigDataQuery.getUsersWithBroadTopicRange(limit.getObject()));
            LOG.info("done! broad interest range query took {} ms", (System.currentTimeMillis() - then));
        }

    }

    private class FocusedRangeQueryPanel extends InterestRangeQueryPanel {
        private FocusedRangeQueryPanel(String id, String title) {
            super(id, title);
        }

        @Override
        protected void doCall() {
            long then = System.currentTimeMillis();
            LOG.info("Beginning calculation for focused interest range ...");
            setData(bigDataQuery.getUsersWithPreciseTopicRange(limit.getObject()));
            LOG.info("done! focused interest range query took {} ms", (System.currentTimeMillis() - then));
        }

    }
}
