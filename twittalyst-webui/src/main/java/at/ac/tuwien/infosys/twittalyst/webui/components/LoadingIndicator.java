package at.ac.tuwien.infosys.twittalyst.webui.components;

import org.apache.wicket.markup.html.panel.Panel;

/**
 * LoadingIndicator
 */
public class LoadingIndicator extends Panel {
    public LoadingIndicator(String id) {
        super(id);
    }
}
