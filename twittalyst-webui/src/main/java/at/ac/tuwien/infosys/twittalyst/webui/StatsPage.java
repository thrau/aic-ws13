package at.ac.tuwien.infosys.twittalyst.webui;

import javax.inject.Inject;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import at.ac.tuwien.infosys.twittalyst.datastore.queries.BigDataQuery;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.UserDAO;
import at.ac.tuwien.infosys.twittalyst.webui.components.ActiveQueryPanel;

/**
 *
 */
public class StatsPage extends BasePage {

    @Inject
    private UserDAO userDAO;

    @Inject
    private BigDataQuery bigDataQuery;

    public StatsPage() {

    }

    public StatsPage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new UserCountPanel("user-count-panel"));
        add(new InteractionsCountPanel("interactions-count-panel"));
        add(new TopicsCountPanel("topics-count-panel"));
    }

    public class UserCountPanel extends ActiveQueryPanel {
        public UserCountPanel(String id) {
            super(id, "User count");
        }

        IModel<Long> count = new Model<>();

        @Override
        protected void onInitialize() {
            super.onInitialize();

            add(new Label("cnt", count));
        }

        @Override
        protected void doCall() {
            count.setObject(userDAO.getCount());
        }
    }

    public class InteractionsCountPanel extends ActiveQueryPanel {
        public InteractionsCountPanel(String id) {
            super(id, "Interactions");
        }

        IModel<Long> count = new Model<>();

        @Override
        protected void onInitialize() {
            super.onInitialize();

            add(new Label("cnt", count));
        }

        @Override
        protected void doCall() {
            count.setObject(bigDataQuery.countRelations());
        }
    }

    public class TopicsCountPanel extends ActiveQueryPanel {
        public TopicsCountPanel(String id) {
            super(id, "Topics");
        }

        IModel<Long> count = new Model<>();

        @Override
        protected void onInitialize() {
            super.onInitialize();

            add(new Label("cnt", count));
        }

        @Override
        protected void doCall() {
            count.setObject(0L); // TODO
        }

        @Override
        public boolean isVisible() {
            return false;
        }
    }
}
