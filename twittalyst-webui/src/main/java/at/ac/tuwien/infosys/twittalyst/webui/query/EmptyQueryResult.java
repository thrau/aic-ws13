package at.ac.tuwien.infosys.twittalyst.webui.query;

/**
 * Static object that represents an empty query result.
 */
public final class EmptyQueryResult<T> implements QueryResult<T> {

    public static final EmptyQueryResult<?> EMPTY_RESULT = new EmptyQueryResult<>();

    private EmptyQueryResult() {

    }

    @Override
    public Class<?> getType() {
        return Void.class;
    }

    @Override
    public T getValue() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @SuppressWarnings("unchecked")
    public static <T> EmptyQueryResult<T> get() {
        return (EmptyQueryResult<T>) EMPTY_RESULT;
    }

}
