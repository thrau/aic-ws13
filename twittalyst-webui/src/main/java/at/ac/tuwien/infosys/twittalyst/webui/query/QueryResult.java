package at.ac.tuwien.infosys.twittalyst.webui.query;

import org.apache.wicket.util.io.IClusterable;

/**
 * Represents the result of a query. Subclass this to specify the structure of the query result.
 */
public interface QueryResult<T> extends IClusterable {

    /**
     * Special case object for empty results.
     */
    public static final EmptyQueryResult<?> EMPTY_RESULT = EmptyQueryResult.EMPTY_RESULT;

    /**
     * Returns the type of the result.
     */
    Class<?> getType();

    /**
     * Returns the actual result value.
     */
    T getValue();

    /**
     * Returns true if this result represents an empty result (i.e. a successful query that returned nothing)
     * 
     * @return true if the result is empty
     */
    boolean isEmpty();
}
