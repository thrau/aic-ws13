package at.ac.tuwien.infosys.twittalyst.webui.components;

import org.apache.wicket.model.IModel;

import java.io.Serializable;

/**
* ColumnValueCallback
*/
public interface ColumnValueCallback<T, S> extends Serializable {
    S call(IModel<T> model);
}
