package at.ac.tuwien.infosys.twittalyst.webui.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.markup.html.panel.GenericPanel;
import org.apache.wicket.model.IModel;

import at.ac.tuwien.infosys.twittalyst.common.util.Callback;
import at.ac.tuwien.infosys.twittalyst.common.util.Collections;
import at.ac.tuwien.infosys.twittalyst.webui.model.UserInfluence;

import com.googlecode.wickedcharts.highcharts.options.Axis;
import com.googlecode.wickedcharts.highcharts.options.ChartOptions;
import com.googlecode.wickedcharts.highcharts.options.Legend;
import com.googlecode.wickedcharts.highcharts.options.Options;
import com.googlecode.wickedcharts.highcharts.options.PlotOptions;
import com.googlecode.wickedcharts.highcharts.options.PlotOptionsChoice;
import com.googlecode.wickedcharts.highcharts.options.SeriesType;
import com.googlecode.wickedcharts.highcharts.options.Stacking;
import com.googlecode.wickedcharts.highcharts.options.Title;
import com.googlecode.wickedcharts.highcharts.options.series.Series;
import com.googlecode.wickedcharts.highcharts.options.series.SimpleSeries;
import com.googlecode.wickedcharts.wicket6.highcharts.Chart;

/**
 * UserInfluenceChart
 */
public class UserInfluenceChart extends GenericPanel<List<UserInfluence>> {
    public UserInfluenceChart(String id, IModel<List<UserInfluence>> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        List<UserInfluence> model = getModelObject();

        Options options = new Options().setChart(new ChartOptions().setType(SeriesType.BAR));

        options.setPlotOptions(new PlotOptionsChoice().setSeries(new PlotOptions().setStacking(Stacking.NORMAL)));
        options.setLegend(new Legend().setReversed(true));

        options.setxAxis(getX(model)).setyAxis(getY(model));
        options.setSeries(getSeries(model));

        Chart chart = new Chart("chart", options.setTitle(new Title("Influence")));

        add(chart);
    }

    private List<Series<?>> getSeries(List<UserInfluence> model) {
        List<Series<?>> list = new ArrayList<>();

        list.add(new SimpleSeries().setName("Interactions").setData(extract(model, "interactions", Number.class)));
        list.add(new SimpleSeries().setName("Tweets").setData(extract(model, "user.tweetCount", Number.class)));
        list.add(new SimpleSeries().setName("Follower").setData(
                Collections.map(model, new Callback<UserInfluence, Number>() {
                    @Override
                    public Number call(UserInfluence object) {
                        return object.getUser().getFollowerCount();
                    }
                })));
        list.add(new SimpleSeries().setName("Friends").setData(extract(model, "user.friendCount", Number.class)));

        return list;
    }

    private <E, R> List<R> extract(Collection<E> collection, final String expression, final Class<R> type) {
        return Collections.map(collection, new Callback<E, R>() {
            @SuppressWarnings("unchecked")
            @Override
            public R call(E object) {
                return type.cast(PropertyResolver.getValue(expression, object));
            }
        });
    }

    private Axis getY(List<UserInfluence> model) {
        Axis y = new Axis();

        y.setMin(0);
        y.setTitle(new Title("Influence"));

        return y;
    }

    private Axis getX(List<UserInfluence> list) {
        Axis x = new Axis();

        x.setCategories(Collections.map(list, new Callback<UserInfluence, String>() {
            @Override
            public String call(UserInfluence object) {
                return object.getUser().getName();
            }
        }));

        return x;
    }

}
