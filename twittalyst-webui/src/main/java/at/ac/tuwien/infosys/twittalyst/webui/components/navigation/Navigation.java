package at.ac.tuwien.infosys.twittalyst.webui.components.navigation;

import java.util.List;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 */
public class Navigation extends Panel {

    private List<NavigationItem> items;

    public Navigation(String id, List<NavigationItem> items) {
        super(id);
        this.items = items;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        ListView<NavigationItem> list = new ListView<NavigationItem>("list", items) {
            @Override
            protected void populateItem(ListItem<NavigationItem> item) {
                WebMarkupContainer container = new WebMarkupContainer("item");
                NavigationLink link = new NavigationLink("link", item.getModelObject());

                container.add(link);
                item.add(container);

                if (link.linksToCurrentPage()) {
                    container.add(new AttributeAppender("class", "active"));
                }
            }
        };

        add(list);
    }

}
