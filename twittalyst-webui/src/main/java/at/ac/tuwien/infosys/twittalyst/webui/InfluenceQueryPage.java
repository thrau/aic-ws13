package at.ac.tuwien.infosys.twittalyst.webui;

import javax.inject.Inject;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.model.Model;

import at.ac.tuwien.infosys.twittalyst.webui.components.ExceptionPanel;
import at.ac.tuwien.infosys.twittalyst.webui.components.LoadingIndicator;
import at.ac.tuwien.infosys.twittalyst.webui.components.QueryForm;
import at.ac.tuwien.infosys.twittalyst.webui.model.UserInfluence;
import at.ac.tuwien.infosys.twittalyst.webui.query.ListQueryResult;
import at.ac.tuwien.infosys.twittalyst.webui.query.QueryParameters;
import at.ac.tuwien.infosys.twittalyst.webui.query.provider.InfluentialUserQueryProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * InfluenceQueryPage
 */
public class InfluenceQueryPage extends BasePage {

    private static final Logger LOG = LoggerFactory.getLogger(InfluenceQueryPage.class);

    @Inject
    InfluentialUserQueryProvider queryProvider;

    WebMarkupContainer resultContainer;

    String resultPanelId = "resultPanel";

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new InfluentialUserQueryForm("queryForm"));

        add(resultContainer = new WebMarkupContainer("resultContainer"));
        resultContainer.setOutputMarkupId(true);

        resultContainer.add(new WebMarkupContainer(resultPanelId));
    }

    class InfluentialUserQueryForm extends QueryForm {

        public InfluentialUserQueryForm(String id) {
            super(id);
        }

        @Override
        protected void populateForm(Form<QueryParameters> form) {
            form.add(new NumberTextField<>("limit").setLabel(Model.of("Limit")).setType(Integer.class));
        }

        @Override
        protected void onSubmit(AjaxRequestTarget target, Form<QueryParameters> form) {
            super.onSubmit(target, form);

            QueryParameters parameters = form.getModelObject();

            Component c;
            try {
                long then = System.currentTimeMillis();
                LOG.info("Computing influence from graph db...");
                ListQueryResult<UserInfluence> run = queryProvider.getQuery().run(parameters);
                LOG.info("done! took {} ms", (System.currentTimeMillis() - then));
                c = queryProvider.getResultRenderer().render(resultPanelId, parameters, run);
            } catch (Exception e) {
                c = new ExceptionPanel(resultPanelId, e);
            }

            resultContainer.replace(c);
            target.add(resultContainer);
        }

        @Override
        protected void onClick(AjaxRequestTarget target) {
            resultContainer.replace(new LoadingIndicator(resultPanelId));
            target.add(resultContainer);
        }
    }

}
