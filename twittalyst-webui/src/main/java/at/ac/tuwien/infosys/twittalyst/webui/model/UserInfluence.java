package at.ac.tuwien.infosys.twittalyst.webui.model;

import org.apache.wicket.util.io.IClusterable;

import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;

/**
 * UserInfluence
 */
public class UserInfluence implements IClusterable {

    private User user;
    private long interactions;

    public UserInfluence() {

    }

    public UserInfluence(User user, long interactions) {
        setUser(user);
        setInteractions(interactions);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getInteractions() {
        return interactions;
    }

    public void setInteractions(long interactions) {
        this.interactions = interactions;
    }
}
