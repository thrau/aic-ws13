package at.ac.tuwien.infosys.twittalyst.webui.components;

import java.util.List;

import org.apache.wicket.markup.html.panel.GenericPanel;
import org.apache.wicket.model.IModel;

import at.ac.tuwien.infosys.twittalyst.webui.model.UserInfluence;

/**
 * UserInfluencePanel
 */
public class UserInfluencePanel extends GenericPanel<List<UserInfluence>> {

    public UserInfluencePanel(String id, IModel<List<UserInfluence>> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new UserInfluenceTable("table", getModel()));
        add(new UserInfluenceChart("chart", getModel()));
    }
}
