package at.ac.tuwien.infosys.twittalyst.webui.query;

/**
 * Holds the service that knows how to run a specific query.
 */
public interface RunnableQuery<T> {

    /**
     * Returns a description of what the query result represents. Shown in the ui.
     */
    String getDescription();

    /**
     * Runs the query and returns a
     * @param parameters
     * @return
     */
    QueryResult<T> run(QueryParameters parameters);
}
