package at.ac.tuwien.infosys.twittalyst.webui;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import at.ac.tuwien.infosys.twittalyst.webui.service.ILabelGenerator;

public class HomePage extends BasePage {
    private static final long serialVersionUID = 1L;

    @SpringBean
    private ILabelGenerator labelGenerator;

    public HomePage() {
        super();
    }

    public HomePage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new Label("foobar", labelGenerator.generateLabel()));
    }

    public ILabelGenerator getLabelGenerator() {
        return labelGenerator;
    }

    public void setLabelGenerator(ILabelGenerator labelGenerator) {
        this.labelGenerator = labelGenerator;
    }

}
