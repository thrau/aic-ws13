package at.ac.tuwien.infosys.twittalyst.webui;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import at.ac.tuwien.infosys.twittalyst.webui.components.navigation.Navigation;
import at.ac.tuwien.infosys.twittalyst.webui.components.navigation.NavigationItem;

public class BasePage extends WebPage {

    private static final long serialVersionUID = 1L;

    @Inject
    @Named("navigationItems")
    private List<NavigationItem> navigationItems;

    public BasePage() {
        super();
    }

    public BasePage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new Navigation("navigation", navigationItems));
    }
}
