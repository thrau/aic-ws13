package at.ac.tuwien.infosys.twittalyst.datastore.docdb;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Eduard Thamm 10/31/13
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:ac-datastore-test.xml" })
public class AdvertisementDAOIT {

    @Autowired
    private AdvertisementDAO AdsDAO;
    @Autowired
    private DBCollection AdsCollection;
    private long NumberOfElementsBefore = 0;

    @Before
    public void setUp() throws Exception {
        //AdsCollection.drop();
        //insertTestValues();
        NumberOfElementsBefore = AdsCollection.count();

    }

    private void insertTestValues() {
        List<String> topic_car = new LinkedList<>();
        List<String> topic_cheese = new LinkedList<>();
        List<String> topic_watches = new LinkedList<>();

        topic_car.add("Car");
        topic_cheese.add("Cheese");
        topic_watches.add("Watch");

        BasicDBObjectBuilder car =
            BasicDBObjectBuilder.start().add("sponsor", "AlphaRomeo").add("text", "We built a new Spider.")
                    .add("topics", topic_car).add("synonyms", topic_car);
        BasicDBObjectBuilder cheese =
            BasicDBObjectBuilder.start().add("sponsor", "HenryWillig").add("text", "We have a new cheese.")
                    .add("topics", topic_cheese).add("synonyms", topic_cheese);
        BasicDBObjectBuilder watch =
            BasicDBObjectBuilder.start().add("sponsor", "Rolex").add("text", "We built a new watch.")
                    .add("topics", topic_watches).add("synonyms", topic_watches);

        AdsCollection.insert(car.get());
        AdsCollection.insert(cheese.get());
        AdsCollection.insert(watch.get());

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetBySponsor() throws Exception {
        String sponsor = "AlphaRomeo";
        List<Advertisement> ads = AdsDAO.getBySponsor(sponsor);

        for (Advertisement ad : ads) {
            Assert.assertEquals("Wrong ad returned.", sponsor, ad.getSponsor());
        }

    }

    @Test
    public void testGetByTopic() throws Exception {
        List<String> topic = new LinkedList<>();
        topic.add("Cheese");
        List<Advertisement> ads = AdsDAO.getByTopic(topic);

        for (Advertisement ad : ads) {
            Assert.assertTrue("Requested topic not in result.", ad.getTopics().contains(topic.get(0)));
        }

    }

    @Test
    public void testAdd() throws Exception {
        List<String> topics = new LinkedList<>();
        topics.add("Auto");
        Advertisement ad = new SimpleAdvertisement("Volkswagen", "Kaufen Sie den neuen VW Golf", topics);
        AdsDAO.add(ad);
        //Assert.assertEquals("Insert in to Database failed.", NumberOfElementsBefore + 1, AdsCollection.count());
    }

    /*@Test
    public void testUpsert() throws Exception {
        String sponsor = "AlphaRomeo";
        Advertisement adBefore = AdsDAO.getBySponsor(sponsor).get(0);
        adBefore.getTopics().add("Fast");

        AdsDAO.upsert(adBefore);

        Advertisement adAfter = AdsDAO.getBySponsor(sponsor).get(0);

        Assert.assertEquals("Uspert failed", adBefore.getTopics(), adAfter.getTopics());

    }*/

    @Test
    public void testDelete() throws Exception {
        List<String> topics = new LinkedList<>();
        topics.add("Auto");
        Advertisement ad = new SimpleAdvertisement("Volkswagen", "Kaufen Sie den neuen VW Golf", topics);
        AdsDAO.add(ad);
        AdsDAO.delete(ad);
        //Assert.assertEquals("Deleting from Database failed", NumberOfElementsBefore, AdsCollection.count());
    }
    
    @Test
    public void testUpdateTopics() throws Exception{
        HashMap<String, HashSet<String>> newTopics = new HashMap<String, HashSet<String>>();
        HashSet<String> set = new HashSet<String>();
        set.add("Auto");
        set.add("Volswagen");
        set.add("BMW");
        newTopics.put("Auto", set);
        AdsDAO.updateTopics(newTopics);
    }
    
    @Test
    public void testGetTopics() throws Exception{
        Map<String, ? extends Set<String>> res = AdsDAO.getTopics();
        for (Map.Entry<String, ? extends Set<String>> entry: res.entrySet()){
            System.out.println(entry.getKey()+": "+entry.getValue().toString());
        }
        Assert.assertTrue("Auto not in Topics", res.containsKey("Auto"));
    }

    @Test
    public void testPictureStorage() throws Exception{
        InputStream pic = getClass().getResourceAsStream("/fashion.jpg");
        List<String> topics = new LinkedList<String>();
        topics.add("Fashion");
        Advertisement ad = new SimpleAdvertisement("Versarchery", "Shooting your foot in 20 languages.", topics, pic);
        AdsDAO.add(ad);
        //Advertisement sad = AdsDAO.getBySponsor("Emi").get(0);
        //Assert.assertNotNull(sad.getPicture());
    }
}
