package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.InteractionRelationRepository;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.InterestedRelationRepository;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.TopicRepository;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:ac-datastore-test.xml" })
public class GraphDbRepositoryIntegrationTest {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private TopicRepository topicRepository;
    
    @Autowired
    private InteractionRelationRepository interactionRepository;
    
    @Autowired
    private InterestedRelationRepository interestedRepository;
    
    @Test @Transactional
    public void testAddUserNodes(){
    	UserNode userNode1 = new UserNode(1L);
    	UserNode userNode2 = new UserNode(2L);
    	
    	userRepository.save(userNode1);
    	userRepository.save(userNode2);
    	
    	assertEquals(2, userRepository.count());
    	assertEquals(userNode1.getUserId(), userRepository.findOne(userNode1.nodeId).getUserId());
    	assertEquals(userNode2.getUserId(), userRepository.findOne(userNode2.nodeId).getUserId());
    }
    
    @Test @Transactional
    public void testAddUserWithDependencies(){
    	UserNode userNode1 = new UserNode(1L);
    	UserNode userNode2 = new UserNode(2L);
    	UserNode userNode3 = new UserNode(3L);
    	
    	TopicNode topic = new TopicNode("TestTopic");
    	topic=topicRepository.save(topic);
    	
    	userNode1.addInterestedRelation(topic);
    	
    	userRepository.save(userNode1);
    	userRepository.save(userNode2);
    	userRepository.save(userNode3);
    	
    	userNode1.addInteractingUser(userNode2, 10L);
    	userRepository.save(userNode1);
    	
    	UserNode test=userRepository.findByUserId(1L);
    	
    	long weight=test.getInteractingUsers().iterator().next().weight;
    	
    	assertEquals(10L, weight);
    	assertEquals(1, test.getInterestedInTopics().size());
    	assertEquals(3, userRepository.count());
    }
    
    @Test @Transactional
    public void testUpdateInteractionWeight() {
        UserNode userNode1 = new UserNode(1L);
        UserNode userNode2 = new UserNode(2L);
        
        userRepository.save(userNode1);
        userRepository.save(userNode2);
        
        userNode1.addInteraction(userNode2);
        userRepository.save(userNode1);
        userNode1 = userRepository.findByUserId(1L);
        assertEquals(1L, userNode1.users.iterator().next().weight.longValue());
        
        userNode1.addInteraction(userNode2);
        InteractsRelation interaction = userNode1.users.iterator().next();
        assertEquals(2L, interaction.weight.longValue());
        assertEquals(userNode2.getNodeId(), interaction.user2.getNodeId());
        
        interactionRepository.save(userNode1.getInteractingUsers());
        userNode1 = userRepository.findByUserId(1L);
        assertEquals("Updating weight failed", 2L, userNode1.users.iterator().next().weight.longValue());
    }
    
    
    @Test @Transactional
    public void testStoreTopics() {
        TopicNode topic1 = new TopicNode("Car");
        TopicNode topic2 = new TopicNode("Cheese");
        TopicNode topic3 = new TopicNode("Watch");
        
        topic1=topicRepository.save(topic1);
        topic2=topicRepository.save(topic2); 
        topic3=topicRepository.save(topic3);        
        
        UserNode userNode = new UserNode(1L);
        userRepository.save(userNode);
        
        userNode.addInterestedRelation(topic1);
        userNode.addInterestedRelation(topic2);
        userRepository.save(userNode);
        
        userNode = userRepository.findByUserId(1L);
        assertEquals(2, userNode.topics.size());
        
        userNode.addInterestedRelation(topic2);
        userNode.addInterestedRelation(topic3);
        userRepository.save(userNode);
        
        userNode = userRepository.findByUserId(1L);
        assertEquals(3, userNode.topics.size());
        
        
        UserNode user2 = new UserNode(2L);
        user2.addInterestedRelation(topic2);
        userRepository.save(user2);
        
        user2 = userRepository.findByUserId(2L);
        assertEquals(1, user2.topics.size());
    } 
    
    @Test @Transactional
    public void testUpdateTopicWeight() {
        TopicNode topic2 = new TopicNode("Cheese");

        topic2=topicRepository.save(topic2); 

        UserNode user2 = new UserNode(2L);
        userRepository.save(user2);
        
        InterestedRelation relation=user2.addInterestedRelation(topic2);
        interestedRepository.save(relation);
        
        relation=user2.addInterestedRelation(topic2);
        interestedRepository.save(relation);
                
        relation=user2.addInterestedRelation(topic2);
        
        interestedRepository.save(relation);
        
        user2 = userRepository.findByUserId(2L);
        assertEquals(new Long(3), user2.getInterestedInTopics().iterator().next().weight);
    } 
    
    
    @After
    public void tearDown(){
    	userRepository.deleteAll();
    }
	
}
