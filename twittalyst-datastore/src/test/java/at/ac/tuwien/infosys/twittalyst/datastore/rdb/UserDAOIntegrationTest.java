package at.ac.tuwien.infosys.twittalyst.datastore.rdb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:ac-datastore-test.xml" })
public class UserDAOIntegrationTest {
    @Autowired
    private UserDAO userDAO;
    
    @Test
    public void testAddUser() {
        long id = 3278l;
        User u = new UserImpl(id, "ArminWolfAdding", 98493L, 422L, 25500L);
        userDAO.add(u);
        
        assertNotNull("Adding of user failed", userDAO.getByID(id));
    }
    
    @Test
    public void testDeleteUser() {
        long id = 123l;
        User u = new UserImpl(id, "ArminWolfDeletion", 98493L, 422L, 25500L);
        userDAO.add(u);
        assertNotNull("Deletion failed because user couldn't be added first", userDAO.getByID(id));
        
        userDAO.delete(u);
        assertNull("Deletion of user failed", userDAO.getByID(id));
    }
    
    @Test
    public void testGetById() {
        long id1 = 36728l;
        long id2 = 37288l;
        String name1 = "ArminWolfByID";
        String name2 = "BarackObamaByID";
        
        User u1 = new UserImpl(id1, name1, 2378L, 223L, 23872L);
        User u2 = new UserImpl(id2, name2, 39307407L, 656270L, 10297L);
        
        userDAO.add(u1);
        userDAO.add(u2);
        
        assertEquals("Fetching user by ID failed", name1, userDAO.getByID(id1).getName());
        assertEquals("Fetching user by ID failed", name2, userDAO.getByID(id2).getName());
    }
    
    @Test
    public void testGetByName() {
        long id1 = 367282l;
        long id2 = 372882l;
        String name1 = "ArminWolfByName";
        String name2 = "BarackObamaByName";
        
        User u1 = new UserImpl(id1, name1, 2378L, 223L, 23872L);
        User u2 = new UserImpl(id2, name2, 39307407L, 656270L, 10297L);
        
        userDAO.add(u1);
        userDAO.add(u2);
        
        assertEquals("Fetching user by name failed", id1, userDAO.getByName(name1).getID());
        assertEquals("Fetching user by name failed", id2, userDAO.getByName(name2).getID());
    }
    
    @Test
    public void testUpateUser(){
        long id1 = 3672;
        
        String name1 = "ArminWolfByName22";

        User u1 = new UserImpl(id1, name1, 2378L, 223L, 23872L);
        
        userDAO.add(u1);
        
        u1.setFollowerCount(10000L);
        u1.setFriendCount(2L);
        u1.setTweetCount(5L);

        userDAO.update(u1);
        
        User dbUser=userDAO.getByID(u1.getID());
    	
        assertEquals(new Long(10000L),dbUser.getFollowerCount());
        assertEquals(new Long(2L),dbUser.getFriendCount());
        assertEquals(new Long(5L),dbUser.getTweetCount());
    }
    
    @Test
    public void testUpsertUser() {
        long userid = 647283l;
        User user = new UserImpl(userid, "Dummy User");
        userDAO.upsert(user);
        
        user = userDAO.getByID(userid);
        assertEquals("Username has not been saved", "Dummy User", user.getName());
        assertEquals("FollowerCount has not been saved", new Long(-1l), user.getFollowerCount());
        assertEquals("FriendCount has not been saved", new Long(-1l), user.getFriendCount());
        assertEquals("TweetCount has not been saved", new Long(-1l), user.getTweetCount());
        
        user = new UserImpl(userid, "Dummy User (updated)");
        user.setFollowerCount(32l);
        user.setFriendCount(64l);
        user.setTweetCount(128l);
        userDAO.upsert(user);
        
        user = userDAO.getByID(userid);
        assertEquals("Username has not been updated", "Dummy User (updated)", user.getName());
        assertEquals("FollowerCount has not been updated", new Long(32l), user.getFollowerCount());
        assertEquals("FriendCount has not been updated", new Long(64l), user.getFriendCount());
        assertEquals("TweetCount has not been updated", new Long(128l), user.getTweetCount());
    }

    @Test
    public void getByIDs_returnsCorrectUsers() throws Exception {
        userDAO.upsert(new UserImpl(42L, "Answer"));
        userDAO.upsert(new UserImpl(43L, "Almost answer"));
        userDAO.upsert(new UserImpl(44L, "Far off"));


        List<User> result = userDAO.getByIDs(Arrays.asList(42L, 44L));

        assertEquals(2, result.size());

        assertEquals("Answer", result.get(0).getName());
        assertEquals("Far off", result.get(1).getName());
    }
    
    @Test
    public void testDeleteNonexistingUser() {
        User user = new UserImpl(3232l, "Ghost", 1L, 0L, 0L);
        userDAO.delete(user);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testAddNull() {
        userDAO.add(null);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testDeleteNull() {
        userDAO.delete(null);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testGetByNullName() {
        System.out.println(userDAO.getByName(null));
    }
}
