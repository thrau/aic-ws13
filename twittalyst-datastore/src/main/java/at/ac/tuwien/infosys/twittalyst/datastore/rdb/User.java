package at.ac.tuwien.infosys.twittalyst.datastore.rdb;

import java.io.Serializable;

/**
 * Represents a Twitter user and associated metadata.
 * @author Robert Horvath
 */
public interface User extends Serializable {
    
    /**
     * 
     * @return The user ID.
     */
    public long getID();
    
    /**
     * 
     * @return The screen name.
     */
    public String getName();
    
    /**
     * 
     * @param name The screen name.
     */
    public void setName(String name);
    
    /**
     * 
     * @return The follower count.
     */
    public Long getFollowerCount();
    
    /**
     * 
     * @param followerCount The follower count.
     */
    public void setFollowerCount(Long followerCount);
    
    /**
     * 
     * @return The number of twitterers this user follows.
     */
    public Long getFriendCount();
    
    /**
     * 
     * @param friendcount The number of twitterers this user follows.
     */
    public void setFriendCount(Long friendCount);
    
    
    /**
     * 
     * @return The number of tweets.
     */
    public Long getTweetCount();
    
    /**
     * 
     * @param tweetCount The number of tweets.
     */
    public void setTweetCount(Long tweetCount);
    
}
