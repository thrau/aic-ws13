package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.TopicRepository;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.UserRepository;

public class RelationsDAO {
    
    @Autowired
    private GraphDatabaseService db;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private TopicRepository topicRepository;
    
    private HashMap<String, TopicNode> topicNodes = new HashMap<String, TopicNode>();
    
    private ExecutionEngine executionEngine;
    
    private final String addUserInteractionRelationsQuery = "START "
        + "n=node({userId}), "
        + "u=node({otherIds})"
        + "CREATE UNIQUE (n)-[i:INTERACTS_WITH]->(u) "
        + "SET i.weight=COALESCE(i.weight?, 0)+1, "
        + "i.__type__= \"" + InteractsRelation.class.getName() + "\" "
        + "RETURN n.userId, u.userId, i.weight";
    
    private final String addUserInterestsRelationsQuery = "START "
        + "n=node({userId}), "
        + "t=node({topics}) "
        + "CREATE UNIQUE (n)-[r:INTERESTED_IN]->(t) "
        + "SET r.weight=COALESCE(r.weight?, 0)+1, "
        + "r.__type__= \"" + InterestedRelation.class.getName() + "\" "
        + "RETURN n.userId, r.weight, t.topicName";
    
    private ExecutionEngine getExecutionEngine() {
        if (executionEngine != null) return executionEngine;
        
        executionEngine = new ExecutionEngine(db);
        return executionEngine;
    }
    
    
    /**
     * Everything in twitter IDs
     * @param userId
     * @param userIds
     * @param topicNames
     */
    @Transactional
    public void addUserWithInteractionsAndInterests(long userId, Set<Long> userIds, List<String> topicNames) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        
        // create user's node if it doesn't exist
        UserNode user = new UserNode(userId);
        userRepository.save(user);
        params.put("userId", user.nodeId);
        
        if (topicNames != null && topicNames.size() > 0) {
            params.put("topics", getTopicIds(topicNames));
            getExecutionEngine().execute(addUserInterestsRelationsQuery, params);
        }
        
        if (userIds != null && userIds.size() > 0) {
            Set<Long> otherIds = getUserIds(userIds);
            otherIds.remove(user.nodeId);
            params.put("otherIds", otherIds);
            getExecutionEngine().execute(addUserInteractionRelationsQuery, params);
        }
    }
    
    private Set<Long> getUserIds(Set<Long> twitterIds) {
        if (twitterIds == null) return null;
        
        Set<Long> userIds = new HashSet<>();
        for (Long uid : twitterIds) {
            UserNode userNode = new UserNode(uid);
            userRepository.save(userNode);
            userIds.add(userNode.nodeId);
        }
        return userIds;
    }
    
    private Set<Long> getTopicIds(List<String> topics) {
        if (topics == null) return null;
        
        Set<Long> topicIds = new HashSet<>();
        for (String topic : topics) {
            TopicNode topicNode = getTopicNode(topic);
            topicIds.add(topicNode.getNodeId());
        }
        return topicIds;
    }
    
    private TopicNode getTopicNode(String topic) {
        if (topic == null) return null;
        
        TopicNode node = topicNodes.get(topic);
        if (node != null) return node;
        
        node = topicRepository.findByPropertyValue("topicName", topic);
        if (node != null) {
            topicNodes.put(topic, node);
            return node;
        }
        
        node = new TopicNode(topic);
        topicRepository.save(node);
        return node;
    }
}
