package at.ac.tuwien.infosys.twittalyst.datastore.queries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.UserInteraction;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.UserImpl;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.index.lucene.ValueContext;
import org.neo4j.kernel.EmbeddedGraphDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import at.ac.tuwien.infosys.twittalyst.datastore.docdb.Advertisement;
import at.ac.tuwien.infosys.twittalyst.datastore.docdb.AdvertisementDAO;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.TopicMention;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.UserTopicMentions;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.TopicRepository;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.UserDAO;
import org.springframework.data.neo4j.annotation.Query;

public class BigDataQuery {
    private static Logger logger = LoggerFactory.getLogger(BigDataQuery.class);

    @Autowired
    private UserDAO userDao;
    
    @Autowired
    private AdvertisementDAO adsDao;
    
    @Autowired
    private TopicRepository topicRepo;
    
    @Autowired
    private EmbeddedGraphDatabase graphDB;
    
    private ExecutionEngine executionEngine;
    
    private ExecutionEngine getExecutionEngine() {
        if (executionEngine != null) return executionEngine;
        
        executionEngine = new ExecutionEngine(graphDB);
        return executionEngine;
    }

    /*
     * (1) Which users are the most influential persons in your data set?
     * Influential persons do not only have many followers, they also trigger many retweets and favorites. 
     */
    public List<Map<String, Object>> getMostInfluentialUsers(int limit) {
        // Get users that other users interact with the most
        // -> those users are often retweeted, often replied to
        //    a lot of people are interested in what they have to say
        String query = "START n=NODE(*) MATCH (m)-[r:INTERACTS_WITH]->(n) RETURN n as user, sum(r.weight) as interactions ORDER BY interactions DESC LIMIT {lim}";
        Map<String, Object> params = new HashMap<>();
        params.put("lim", limit);
        
        ExecutionResult result = getExecutionEngine().execute(query, params);
        ResourceIterator<Map<String, Object>> it = result.iterator();
        
        List<Map<String, Object>> list = new ArrayList<>();
        while (it.hasNext()) {
            Map<String, Object> res = it.next();
            Node userNode = (Node) res.get("user");
            User user = userDao.getByID((Long) userNode.getProperty("userId"));
            
            Map<String, Object> entry = new HashMap<>();
            entry.put("user", user);
            entry.put("interactions", res.get("interactions"));
            
            list.add(entry);
        }
        
        return list;
    }
    
    /*
     * (2_a) Which users are interested in a broad range of topics?
     */

    /**
     * @param limit maximum number of Users returned
     * @return A map of Users, each user has a map of topics with the number of mentions as value for each topic
     */
    
    public List<UserTopicMentions> getUsersWithBroadTopicRange(int limit){
    	if(limit==0){
    		limit=1;
    	}
    	
    	String query="START m=NODE(*) "+
    			"MATCH (m)-[r:INTERESTED_IN]->(t) "+
    			"WITH m,avg(r.weight) as avg "+
    			"MATCH (m)-[r:INTERESTED_IN]->(t) "+
    			"WHERE r.weight>=avg*0.3 "+
    			"WITH m,count(r) as edgecount, sum(r.weight) as totalweight "+
    			"RETURN m as user "+
    			"ORDER BY edgecount DESC,totalweight DESC LIMIT {lim}";

    	
        Map<String, Object> params = new HashMap<>();
        params.put("lim", limit);
    	
        ExecutionResult result = getExecutionEngine().execute(query, params);
        ResourceIterator<Map<String, Object>> it = result.iterator();
        
        List<UserTopicMentions> userTopicMentionsList=new ArrayList<UserTopicMentions>();
        
        while (it.hasNext()) {
            Map<String, Object> res = it.next();
            Node userNode = (Node) res.get("user");
            User user = userDao.getByID((Long) userNode.getProperty("userId"));
            
            List<TopicMention> topicMentionsList=topicRepo.getTopicsOfUserWithNumberOfMentions(userNode.getId());

            userTopicMentionsList.add(new UserTopicMentions(user.getName(),topicMentionsList));
        }
 
    	return userTopicMentionsList;
    }
    

    
    
    /*
     * (2_b) Which users are focused on a precise range of topics?
     */
    
    public List<UserTopicMentions> getUsersWithPreciseTopicRange(int limit){
    	if(limit==0){
    		limit=1;
    	}
    	
    	String query="START m=NODE(*) "+
    			"MATCH (m)-[r:INTERESTED_IN]->(t) "+
    			"WITH m,avg(r.weight) as avg "+
    			"MATCH (m)-[r:INTERESTED_IN]->(t) "+
    			"WHERE r.weight>=avg*0.3 "+
    			"WITH m,count(r) as edgecount,sum(r.weight) as totalweight "+
    			"RETURN m as user "+
    			"ORDER BY edgecount,totalweight DESC LIMIT {lim}";
    	

        Map<String, Object> params = new HashMap<>();
        params.put("lim", limit);
    	
        ExecutionResult result = getExecutionEngine().execute(query, params);
        ResourceIterator<Map<String, Object>> it = result.iterator();
        
        List<UserTopicMentions> userTopicMentionsList=new ArrayList<UserTopicMentions>();
        
        while (it.hasNext()) {
        	
        	
            Map<String, Object> res = it.next();
            Node userNode = (Node) res.get("user");
            User user = userDao.getByID((Long) userNode.getProperty("userId"));
            
            List<TopicMention> topicMentionsList=topicRepo.getTopicsOfUserWithNumberOfMentions(userNode.getId());

            userTopicMentionsList.add(new UserTopicMentions(user.getName(),topicMentionsList));
        }
 
    	return userTopicMentionsList;
    }

    
    /*
     * (3) Suggest concrete ads from your database to a given user based on his existing interests, i.e., topics he is already mentioning actively. 
     */
    public List<String> suggestTopicsForUser(long twitterId) {
        String query = "START "
                + "n=node:UserNode(userId={uid}) "
                + "MATCH (n)-[r:INTERESTED_IN]->(t) "
                + "RETURN t.topicName as topic, sum(r.weight) as weight "
                + "ORDER BY weight DESC, topic";
            
            Map<String, Object> params = new HashMap<>();
            params.put("uid", ValueContext.numeric(twitterId));
            
            ExecutionResult result = getExecutionEngine().execute(query, params);
            ResourceIterator<Map<String, Object>> it = result.iterator();
            
            List<String> topics = new ArrayList<>();
            while (it.hasNext()) {
                topics.add(it.next().get("topic").toString());
            }
            return topics;
    }
    
    public List<String> suggestTopicsForUser(String name) {
        User user = userDao.getByName(name);
        if (user == null) return null;
        
        return suggestTopicsForUser(user.getID());
    }
    
    public List<Advertisement> suggestAdsForUser(long twitterId) {
        return adsDao.getByTopic(suggestTopicsForUser(twitterId));
    }
    
    public List<Advertisement> suggestAdsForUser(String name) {
        return adsDao.getByTopic(suggestTopicsForUser(name));
    }
    
    
    /*
     * (4) Suggest concrete ads from your database to a given user based on his potential interests, i.e., topics he does not actively mention at the moment,
     * but which are his connections (and their friends, and their friends, ..., with decreasing importance) are interested in.
     */
    /**
     * @param twitterId the id of the user to suggest topics for
     * @param maxDistance the maximum distance of related users whose interests should be considered
     * @return A list of suggested topics for a user
     */
    public List<String> suggestTopicsForUserBasedOnInteractions(long twitterId, int maxDistance) {
        if (maxDistance <= 1) maxDistance = 4;
        
        //String query = "START "
        //    + "n=node:UserNode(userId={uid}) "
        //    + "MATCH p=(n)-[r:INTERACTS_WITH*1.." + maxDistance + "]->(u) "
        //    + "WITH n, u, min(length(p)) AS distance "
        //    + "MATCH (u)-[r:INTERESTED_IN]->(t) "
        //    + "WHERE NOT (n-[:INTERESTED_IN]->t) "
        //    + "WITH t, r.weight/distance AS weightedDist "
        //    + "RETURN t.topicName as topic, sum(weightedDist) as weight "
        //    + "ORDER BY weight DESC";
        
        String query = "START "
                + "n=node:UserNode(userId={uid}) "
                + "MATCH p=n-[r:INTERACTS_WITH]->m-[s:INTERACTS_WITH*0.." + (maxDistance-1) + "]->u "
                + "WITH n, u, r.weight*1.0/2^(length(p)-1) as weightdist "
                + "MATCH u-[r:INTERESTED_IN]->t "
                + "WHERE NOT (n-[:INTERESTED_IN]->t) "
                + "WITH t, weightdist*r.weight as total "
                + "RETURN t.topicName as topic, sum(total) as total "
                + "ORDER BY total DESC";
        
        Map<String, Object> params = new HashMap<>();
        params.put("uid", ValueContext.numeric(twitterId));
        
        ExecutionResult result = getExecutionEngine().execute(query, params);
        ResourceIterator<Map<String, Object>> it = result.iterator();
        
        List<String> topics = new ArrayList<>();
        while (it.hasNext()) {
            topics.add(it.next().get("topic").toString());
        }
        return topics;
    }
    
    public List<String> suggestTopicsForUserBasedOnInteractions(String name, int maxDistance) {
        User user = userDao.getByName(name);
        if (user == null) return null;
        
        return suggestTopicsForUserBasedOnInteractions(user.getID(), maxDistance);
    }
    
    public List<Advertisement> suggestAdsForUserBasedOnInteractions(long twitterId, int maxDistance) {
        return adsDao.getByTopic(suggestTopicsForUserBasedOnInteractions(twitterId, maxDistance));
    }
    
    public List<Advertisement> suggestAdsForUserBasedOnInteractions(String name, int maxDistance) {
        return adsDao.getByTopic(suggestTopicsForUserBasedOnInteractions(name, maxDistance));
    }

    public long countRelations() {
        String query = "START m=NODE(*) MATCH (m)-[r:INTERACTS_WITH]->() RETURN COUNT(*) as cnt";
        return (long) getExecutionEngine().execute(query).columnAs("cnt").next();
    }

    public List<UserInteraction> outgoingUserInteractions(long userId) {
        String query =
            "START n=node:UserNode(userId={uid}) MATCH n-[r:INTERACTS_WITH]->u "
                + "RETURN r.weight as interactions, u.userId as uid ORDER BY interactions DESC";

        Map<String, Object> params = new HashMap<>();
        params.put("uid", ValueContext.numeric(userId));

        ExecutionResult execute = getExecutionEngine().execute(query, params);

        ResourceIterator<Map<String, Object>> it = execute.iterator();

        List<UserInteraction> list = new ArrayList<>();

        while (it.hasNext()) {
            Map<String, Object> res = it.next();

            User user = userDao.getByID((Long) res.get("uid"));
            list.add(new UserInteraction(user, (Long) res.get("interactions")));
        }

        return list;
    }

    public static void main(String[] args) {
        /* Init Spring */
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("ac-datastore.xml");
        
        BigDataQuery bdq = (BigDataQuery) applicationContext.getBean(BigDataQuery.class);
        
        if (args.length == 0) {
         //   System.out.println("10 most influential users:");
         //   System.out.println(bdq.getMostInfluentialUsers(10));
            
            System.out.println("Users with broad range of topics:");
            for(UserTopicMentions userTopicMentions:bdq.getUsersWithBroadTopicRange(5)){
            	System.out.println(userTopicMentions.getUserName() + ":");
            	for(TopicMention topicMention: userTopicMentions.getTopicMentionList()){
            		System.out.println(" --- "+ topicMention.getTopicName() + ": "+topicMention.getMentions());
            	}
            }
            
            System.out.println("Users with precise range of topics:");
            
            for(UserTopicMentions userTopicMentions:bdq.getUsersWithPreciseTopicRange(5)){
            	System.out.println(userTopicMentions.getUserName() + ":");
            	for(TopicMention topicMention: userTopicMentions.getTopicMentionList()){
            		System.out.println(" --- "+ topicMention.getTopicName() + ": "+topicMention.getMentions());
            	}
            }
            
            
            
        } else {
            long id;
            try {
                id = Long.valueOf(args[0]);
                
                System.out.println("Suggested Topics:");
                System.out.println(bdq.suggestTopicsForUser(id));
                System.out.println("Suggested Ads:");
                System.out.println(bdq.suggestAdsForUser(id));
                
                System.out.println("Suggested Topics based on Interactions:");
                System.out.println(bdq.suggestTopicsForUserBasedOnInteractions(id, 4));
                System.out.println("Suggested Ads:");
                System.out.println(bdq.suggestAdsForUserBasedOnInteractions(id, 4));
                
            } catch (NumberFormatException e) {
                String name = args[0];
                
                System.out.println("Suggested Topics:");
                System.out.println(bdq.suggestTopicsForUser(name));
                System.out.println("Suggested Ads:");
                System.out.println(bdq.suggestAdsForUser(name));
                
                System.out.println("Suggested Topics based on Interactions:");
              // System.out.println(bdq.suggestTopicsForUserBasedOnInteractions(name));
                System.out.println("Suggested Ads:");
              //  System.out.println(bdq.suggestAdsForUserBasedOnInteractions(name));
            }
        }
    }

}
