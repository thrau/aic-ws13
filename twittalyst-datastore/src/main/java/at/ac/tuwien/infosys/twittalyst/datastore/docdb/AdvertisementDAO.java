package at.ac.tuwien.infosys.twittalyst.datastore.docdb;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Eduard Thamm 10/31/13
 */
public interface AdvertisementDAO {

    public List<Advertisement> getAll();

    public List<Advertisement> getBySponsor(String sponsor);

    public List<Advertisement> getByTopic(List<String> topic);

    public void add(Advertisement advertisement);

    public void upsert(Advertisement advertisement);

    public void delete(Advertisement advertisement);

    public Map<String, ? extends Set<String>> getTopics();

    void updateTopics(Map<String, ? extends Set<String>> newTopics);

}
