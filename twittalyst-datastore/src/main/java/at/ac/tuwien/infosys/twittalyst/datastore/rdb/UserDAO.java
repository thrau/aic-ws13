package at.ac.tuwien.infosys.twittalyst.datastore.rdb;

import java.util.Collection;
import java.util.List;


public interface UserDAO {
    
    /**
     * 
     * @param id The Twitter ID
     * @return The user with the given Twitter ID
     */
    public User getByID(long id);

    /**
     *
     * @param ids a list of Twitter IDs
     * @return Users with the given Twitter IDs
     */
    List<User> getByIDs(Collection<Long> ids);

    /**
     * 
     * @param name The user's name.
     * @return The user with the given name.
     */
    public User getByName(String name);

    /**
     * @return The amount of users in the database
     */
    public long getCount();

    /**
     * Stores the given user to the database. 
     * @param user The user to add to the database.
     */
    public void add(User user);
    
    /**
     * Deletes a user from the database.
     * @param user The user to delete from the database.
     */
    public void delete(User user);
    
    /**
     * Updates followerCount, friendCount and tweetCount of an existing user in the database 
     * @param user The user to delete from the database.
     */
    public void update(User user);
    
    /**
     * Updates the User in the database, or inserts her if not yet in the database  
     * @param user The user to update/insert
     */
    public void upsert(User user);
    
    /**
     * Updates the Users in the database, or inserts them if not yet in the database  
     * @param users The users to update/insert
     */
    public void batchUpsert(List<User> users);
    
}
