package at.ac.tuwien.infosys.twittalyst.datastore.rdb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class UserJdbcDAO implements UserDAO {
    private static Logger logger = LoggerFactory.getLogger(UserJdbcDAO.class);
    
    @Autowired
    private DataSource dataSource;
    
    private static final UserRowMapper userRowMapper = new UserRowMapper();
    private static final String queryGetByID = "SELECT * FROM USERS WHERE ID = ?";
    private static final String queryGetByIDs = "SELECT * FROM USERS WHERE ID IN (:IDS)";
    private static final String queryGetByName = "SELECT * FROM USERS WHERE NAME LIKE ? LIMIT 1";
    private static final String queryGetCount = "SELECT COUNT(*) FROM USERS";
    private static final String queryAdd = "INSERT INTO USERS (ID, NAME, FOLLOWERS, FRIENDS, TWEETS) VALUES (:ID, :NAME, :FOLLOWERS, :FRIENDS, :TWEETS)";
    private static final String queryUpdate = "UPDATE USERS SET NAME=:NAME, FOLLOWERS=:FOLLOWERS, FRIENDS=:FRIENDS, TWEETS=:TWEETS WHERE ID=:ID";
    private static final String queryDelete = "DELETE FROM USERS WHERE ID = ?";
    private static final String queryUpsertFull = "MERGE INTO USERS (ID, NAME, FOLLOWERS, FRIENDS, TWEETS) VALUES (:ID, :NAME, :FOLLOWERS, :FRIENDS, :TWEETS)";
    private static final String queryUpsertPartial = "MERGE INTO USERS (ID, NAME) VALUES (:ID, :NAME)";
    
    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedTemplate;
    
    @PostConstruct
    private void init() throws SQLException {
        template = new JdbcTemplate(dataSource);
        namedTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public User getByID(long id) {
        logger.trace("Retrieving user by ID (" + id + ")");
        
        try {
            return template.queryForObject(queryGetByID, userRowMapper, id);
        } catch (EmptyResultDataAccessException ex) {
            logger.trace("No user found");
            return null;
        }
    }

    @Override
    public List<User> getByIDs(Collection<Long> ids) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("IDS", ids);

        return namedTemplate.query(queryGetByIDs, parameters, userRowMapper);
    }

    @Override
    public User getByName(String name) {
        if (name == null) throw new IllegalArgumentException("Name required");
        logger.trace("Retrieving user by Name (" + name + ")");
        
        try {
            return template.queryForObject(queryGetByName, userRowMapper, name);
        } catch (EmptyResultDataAccessException ex) {
            logger.trace("No user found");
            return null;
        }
    }

    @Override
    public long getCount() {
        return template.queryForObject(queryGetCount, Long.class);
    }

    @Override
    public void add(User user) {
        if (user == null) throw new IllegalArgumentException("User to add required");
        logger.info("Adding user to rdb: " + user.getName() + " (" + user.getID() + ")");
        logger.trace(user.toString());
        
        namedTemplate.update(queryAdd, getParamMap(user));
    }

    @Override
    public void delete(User user) {
        if (user == null) throw new IllegalArgumentException("User to delete required");
        logger.info("Deleting user from rdb");
        logger.trace(user.toString());
        
        template.update(queryDelete, user.getID());
    }
    
	@Override
	public void update(User user) {
        if (user == null) throw new IllegalArgumentException("User to update required");
        logger.info("Updating user in rdb: " + user.getName() + " (" + user.getID() + ")");
        logger.trace(user.toString());
        
        namedTemplate.update(queryUpdate, getParamMap(user));
		
	}

    @Override
    public void upsert(User user) {
        if (user == null) throw new IllegalArgumentException("User to update required");
        logger.trace("Upserting user in rdb: " + user.getName() + " (" + user.getID() + ")");
        
        if (user.getFollowerCount() != -1)
            namedTemplate.update(queryUpsertFull, getParamMap(user));
        else
            namedTemplate.update(queryUpsertPartial, getParamMap(user));
        
    }
    
    @Override
    public void batchUpsert(List<User> users) {
        for (User user : users) {
            upsert(user);
        }
    }
    
    private Map<String, Object> getParamMap(User user) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ID", user.getID());
        map.put("NAME", user.getName());
        map.put("FOLLOWERS", user.getFollowerCount());
        map.put("FRIENDS", user.getFriendCount());
        map.put("TWEETS", user.getTweetCount());
        return map;
    }
    
    private static class UserRowMapper implements RowMapper<User> {
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            long id = rs.getLong("ID");
            String name = rs.getString("NAME");            
            Long followerCount = rs.getLong("FOLLOWERS");
            Long friendCount = rs.getLong("FRIENDS");
            Long tweetCount = rs.getLong("TWEETS");
            return new UserImpl(id, name, followerCount, friendCount, tweetCount);
        }
    }
}
