package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import java.io.Serializable;

@QueryResult
public interface TopicMention extends Serializable {
    @ResultColumn("topicName")
    public String getTopicName();

    @ResultColumn("mentions")
    public Long getMentions();
}
