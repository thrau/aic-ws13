package at.ac.tuwien.infosys.twittalyst.datastore.docdb;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

/**
 * Eduard Thamm 10/31/13
 */
public interface Advertisement extends Serializable {

    /**
     * 
     * @return The name of the Sponsor.
     */
    public String getSponsor();

    /**
     * 
     * @return The ad text as supplied by the sponsor.
     */
    public String getAdText();

    /**
     * 
     * @return A List of keywords as supplied by the sponsor.
     */
    public List<String> getTopics();

    /**
     * 
     * @return A List of synonyms for the Keywords.
     */
    public List<String> getKeywords();

    /**
     * 
     * @return The id of the Object in the Database
     */
    public String getId();

    /**
     *
     * @return The picture of this advertisement or null
     */
    public InputStream getPicture();

    /**
     * Refreshed the Synonym-List.
     */
    public void refreshSynonyms();

}
