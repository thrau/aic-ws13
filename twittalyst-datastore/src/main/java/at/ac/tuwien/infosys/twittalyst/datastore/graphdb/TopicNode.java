package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;

import java.io.Serializable;

@NodeEntity
public class TopicNode implements Serializable {

	@GraphId Long nodeId;
	@Indexed(unique=true) String topicName;
	
	public TopicNode(){}
	
	public TopicNode(String topicName){
		this.topicName=topicName;
	}
	
	public Long getNodeId(){
		return nodeId;
	}
	
	public String getTopicName(){
		return topicName;
	}
}
