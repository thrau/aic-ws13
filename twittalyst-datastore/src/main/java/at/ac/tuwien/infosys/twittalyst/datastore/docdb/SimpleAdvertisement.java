package at.ac.tuwien.infosys.twittalyst.datastore.docdb;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Eduard Thamm 11/1/13
 */
public class SimpleAdvertisement implements Advertisement {

    private List<String> topics;
    private List<String> keywords;
    private String sponsor;
    private String text;
    private String id;
    private transient InputStream picture;

    public SimpleAdvertisement(String sponsor, String text, List<String> topics) {
        this.sponsor = sponsor;
        this.text = text;
        this.topics = topics;
        this.keywords = null;
        this.id = "";
        this.picture = null;
    }

    public SimpleAdvertisement(String sponsor, String text, List<String> topics, InputStream pic) {
        this.sponsor = sponsor;
        this.text = text;
        this.topics = topics;
        this.keywords = null;
        this.id = "";
        this.picture = pic;
    }

    SimpleAdvertisement(String sponsor, String text, List<String> topics, List<String> keywords, String id, InputStream pic) {
        this.sponsor = sponsor;
        this.text = text;
        this.topics = topics;
        this.keywords = SimpleAdvertisement.turnToLowercase(keywords);
        this.id = id;
        this.picture = pic;
    }
    
    private static List<String> turnToLowercase(List<String> keywords){
        LinkedList<String> lower = new LinkedList<String>();
        for (String s: keywords){
            lower.add(s.toLowerCase());
        }
        return lower;
    }

    @Override
    public String getSponsor() {
        return sponsor;
    }

    @Override
    public String getAdText() {
        return text;
    }

    @Override
    public List<String> getTopics() {
        return topics;
    }

    @Override
    public List<String> getKeywords() {
        if (keywords == null) {
            keywords = SynonymResolver.getSynonyms(topics);
            keywords = SimpleAdvertisement.turnToLowercase(keywords);
        }
        return keywords;

    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public InputStream getPicture() {
        return picture;
    }

    @Override
    public void refreshSynonyms() {
        keywords = SynonymResolver.getSynonyms(topics);
    }

    public String toString() {
        return String.format("SimpleAdvertisment\nSponsor: %s\nText: %s\nTopics: %s\nSynonyms: %s\nID: %s\nPic: %s", sponsor,
                text, topics, keywords, id, picture);
    }

}
