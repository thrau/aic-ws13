package at.ac.tuwien.infosys.twittalyst.datastore.docdb;

import java.util.List;

/**
 * Eduard Thamm 10/31/13
 */
public interface CampaignDAO {

    public List<Campaign> getByName(String name);

    public List<Campaign> getByTopic(String topic);

    public List<Campaign> getAllThatContain(Advertisement advertisement);

    public void add(Campaign campaign);

    public void upsert(Campaign campaign);

    public void delete(Campaign campaign);

}
