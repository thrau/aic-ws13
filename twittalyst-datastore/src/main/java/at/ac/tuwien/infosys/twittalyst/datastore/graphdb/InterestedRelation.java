package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import java.io.Serializable;

@RelationshipEntity(type="INTERESTED_IN")
public class InterestedRelation implements Serializable {
	@GraphId Long relationId;
	@StartNode public UserNode user;
	@EndNode @Fetch public TopicNode topic;
	public Long weight;
	
	public InterestedRelation(){}
	
	public InterestedRelation(UserNode user, TopicNode topic, Long weight){
		this.user=user;
		this.topic=topic;
		this.weight=weight;
	}
	
	@Override
	public String toString() {
	    return String.format("Interested %s -> %s, weight %d",  user.toString(), topic.toString(), weight) ;
	}
}
