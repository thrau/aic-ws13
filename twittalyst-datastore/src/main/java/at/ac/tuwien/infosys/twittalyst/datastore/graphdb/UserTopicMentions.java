package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import java.io.Serializable;
import java.util.List;

public class UserTopicMentions implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1497955499995439385L;
	
	private String userName;
	private List<TopicMention> topicMentionList;
	
	public UserTopicMentions(String userName, List<TopicMention> topicMentionList){
		this.userName=userName;
		this.topicMentionList=topicMentionList;
	}
	
	public String getUserName(){
		return userName;
	}
	
	public List<TopicMention> getTopicMentionList(){
		return topicMentionList;
	}
	
	
	public String toString(){
		return userName + ": " + topicMentionList;
	}
	
}
