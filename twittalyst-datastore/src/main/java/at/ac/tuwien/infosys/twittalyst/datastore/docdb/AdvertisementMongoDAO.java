package at.ac.tuwien.infosys.twittalyst.datastore.docdb;

import com.mongodb.*;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.mongodb.util.StringBuilderPool;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Eduard Thamm 10/31/13
 */
@SuppressWarnings("unchecked")
public class AdvertisementMongoDAO implements AdvertisementDAO {

    Logger logger = LoggerFactory.getLogger(AdvertisementMongoDAO.class);
    @Autowired
    private MongoClient mongo;
    @Autowired
    private DB mongoDb;
    @Autowired
    private DBCollection AdsCollection;
    @Autowired
    private DBCollection TopicsCollection;
    @Autowired
    private GridFS PictureFS;

    public List<Advertisement> getAll() {
        BasicDBObjectBuilder query = BasicDBObjectBuilder.start();
        DBCursor cursor = AdsCollection.find(query.get());

        logger.debug("DB returned {} elements", cursor.count());

        return constructListFromCursor(cursor);
    }

    @Override
    public List<Advertisement> getBySponsor(String sponsor) {
        logger.info("Searching by sponsor");
        logger.debug(sponsor);

        BasicDBObjectBuilder query = BasicDBObjectBuilder.start().add("sponsor", sponsor);
        DBCursor cursor = AdsCollection.find(query.get());

        logger.debug("DB returned {} elements", cursor.count());

        List<Advertisement> result = constructListFromCursor(cursor);

        return result;
    }

    private List<Advertisement> constructListFromCursor(DBCursor cursor) {
        logger.debug("Constructing list from cursor");
        LinkedList<Advertisement> result = new LinkedList<>();

        for (DBObject object : cursor) {

            Advertisement ad = constructAdvertisement(object);

            result.add(ad);
        }

        return result;
    }

    private Advertisement constructAdvertisement(DBObject object) {
        logger.debug("Constructing ad from DBObject.");
        String sponsor = (String) object.get("sponsor");
        String text = (String) object.get("text");
        String id = String.valueOf((ObjectId) object.get("_id"));
        List<String> topics = (List<String>) object.get("topics");
        List<String> synonyms = (List<String>) object.get("synonyms");
        InputStream pic = retreivePicture(object);

        return new SimpleAdvertisement(sponsor, text, topics, synonyms, id, pic);
    }

    private InputStream retreivePicture(DBObject object) {
        String id = (String) object.get("pic");
        if (id == null){
            return null;
        }
        GridFSDBFile out = PictureFS.findOne(new ObjectId(id));
        return out.getInputStream();
    }

    @Override
    public List<Advertisement> getByTopic(List<String> topic) {
        logger.info("Searching by topic");
        logger.debug(topic.toString());

        DBObject query = QueryBuilder.start("topics").in(topic).get();

        DBCursor cursor = AdsCollection.find(query);

        logger.debug("DB returned {} elements", cursor.count());

        List<Advertisement> result = constructListFromCursor(cursor);
        return result;
    }

    @Override
    public void add(Advertisement advertisement) {
        logger.info("Adding advertisement");
        logger.debug(advertisement.toString());
        DBObject ad = constructDBObjectFromAd(advertisement);
        AdsCollection.insert(ad);
        Map<String, ? extends Set<String>> newTopics = constructTopicMap(advertisement);
        updateTopics(newTopics);
    }

    private DBObject constructDBObjectFromAd(Advertisement advertisement) {
        String filename = null;
        if (advertisement.getPicture() != null){
            filename = storePicture(advertisement);
            logger.info("Stored "+filename);
        }
        return BasicDBObjectBuilder.start().add("sponsor", advertisement.getSponsor())
                .add("text", advertisement.getAdText()).add("topics", advertisement.getTopics())
                .add("synonyms", advertisement.getKeywords()).add("pic", filename).get();
    }

    private String storePicture(Advertisement advertisement) {
        logger.info("Storing picture");
        GridFSInputFile in = PictureFS.createFile(advertisement.getPicture());
        in.save();
        return in.getId().toString();
    }

    private Map<String, Set<String>> constructTopicMap(Advertisement advertisement) {
        Map<String, Set<String>> map = new HashMap<String, Set<String>>();
        
        for(String topic: advertisement.getTopics()){
            Set<String> keywordSet = new HashSet<String>();
            keywordSet.addAll(advertisement.getKeywords());
            map.put(topic, keywordSet);
        }
        
        return map;
    }
    
    @Override
    public void updateTopics(Map<String, ? extends Set<String>> newTopics){
        //DISABLED due to no show
        /*for(Entry<String, ? extends Set<String>> entry: newTopics.entrySet()){
            DBObject query = BasicDBObjectBuilder.start().add(entry.getKey(), new BasicDBObject("$exists",true)).get();
            DBObject topic = TopicsCollection.findOne(query);
            topic.toMap().entrySet().add(entry.getValue());
            TopicsCollection.update(query, topic, true, false);
        }*/
    }
    


    @Override
    public void upsert(Advertisement advertisement) {
        ObjectId id = new ObjectId(advertisement.getId());
        DBObject query = BasicDBObjectBuilder.start().add("_id", id).get();

        DBObject newad = constructDBObjectFromAd(advertisement);

        AdsCollection.update(query, newad, true, false);
    }

    @Override
    public void delete(Advertisement advertisement) {
        logger.info("Deleting advertisement");
        logger.debug(advertisement.toString());
        BasicDBObjectBuilder ad =
            BasicDBObjectBuilder.start().add("sponsor", advertisement.getSponsor())
                    .add("text", advertisement.getAdText()).add("topics", advertisement.getTopics());
        AdsCollection.remove(ad.get());
    }

    @Override
    public Map<String, ? extends Set<String>> getTopics() {
        HashMap<String, HashSet<String>> result = new HashMap<String, HashSet<String>>();
        DBCursor cursor = TopicsCollection.find();
        
        for (DBObject obj: cursor){
            obj.removeField("_id");
            Map<String, HashSet<String>> tmp = createTopicMapFromObject(obj);
            result.putAll(tmp);
        }
        
        return result;
    }

    private Map<String, HashSet<String>> createTopicMapFromObject(DBObject obj) {
        HashMap<String, HashSet<String>> result = new HashMap<String, HashSet<String>>();
        
        for (String key: obj.keySet()){
            HashSet<String> s = new HashSet<String>();
            s.addAll((Collection<? extends String>) obj.get(key));
            result.put(key, s);
        }
        
        return result;
    }
    
}
