package at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.TopicMention;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.TopicNode;


public interface TopicRepository extends GraphRepository<TopicNode>  {

    @Query( "start user=node({0})" +
            "match user-[r:INTERESTED_IN]->t " +
            "return t.topicName as topicName,r.weight as mentions")
	List<TopicMention> getTopicsOfUserWithNumberOfMentions(Long graphId);


    @Query( "start user=node(*)" +
            "match user-[r:INTERESTED_IN]->t " +
            "where has(user.userId) AND user.userId={0} " +
            "return t.topicName as topicName,r.weight as mentions")
    List<TopicMention> getTopicsForUser(Long userId);
}
