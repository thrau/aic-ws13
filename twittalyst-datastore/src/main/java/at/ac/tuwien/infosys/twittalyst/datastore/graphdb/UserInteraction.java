package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import java.io.Serializable;

import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;

/**
 * UserInteraction
 */
public class UserInteraction implements Serializable {

    private User user;

    private long interactions;

    public UserInteraction() {
    }

    public UserInteraction(User user, long interactions) {
        this.user = user;
        this.interactions = interactions;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getInteractions() {
        return interactions;
    }

    public void setInteractions(long interactions) {
        this.interactions = interactions;
    }
}
