package at.ac.tuwien.infosys.twittalyst.datastore.docdb;

import java.util.List;

/**
 * Eduard Thamm 10/31/13
 */
public interface Campaign {

    /**
     * 
     * @return The name of the campaign.
     */
    public String getName();

    /**
     * 
     * @return A list of advertisements in this campaign.
     */
    public List<Advertisement> getAdvertisements();

    /**
     * 
     * @return One random advertisement of this campaign.
     */
    public Advertisement getOneAdvertisment();

    /**
     * 
     * @return A list of keywords supplied by the advertisements in the campaign.
     */
    public List<String> getTopics();

    /**
     * 
     * @return A list of synonyms supplied by the advertisements in this campaign.
     */
    public List<String> getSynonyms();
}
