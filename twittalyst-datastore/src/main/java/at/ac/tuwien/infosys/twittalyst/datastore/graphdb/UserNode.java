package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.annotation.RelatedToVia;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.InteractionRelationRepository;

@NodeEntity
public class UserNode implements Serializable {
	@GraphId Long nodeId;
	@Indexed(unique=true) private Long userId;
	
	@RelatedToVia(type = "INTERACTS_WITH", direction = Direction.OUTGOING)
	Set<InteractsRelation> users;
	
	@RelatedToVia(type = "INTERESTED_IN", direction = Direction.OUTGOING)
	Set<InterestedRelation> topics;
	
	@Autowired
	transient InteractionRelationRepository interactionsRepository;
	
	@Query("START m=node({self}) MATCH m-[r:INTERACTS_WITH]->user RETURN r.weight as weight, ID(user) as nodeId, user.userId as userId")
    public Iterable<Map<String, Object>> interactions;
	
	public UserNode(){}
	
	public UserNode(Long userId){
		this.userId=userId;
	}
	
	public Long getNodeId(){
		return nodeId;
	}
	
	public Long getUserId(){
	    return userId;
	}
	
	public InteractsRelation addInteractingUser(UserNode userNode, Long weight){
		if(users==null){
			users=new HashSet<InteractsRelation>();
		}
		
		InteractsRelation interact=new InteractsRelation(this,userNode,weight);
		users.add(interact);
		
		return interact;
	}
	
	public InteractsRelation addInteraction(UserNode userNode) {
	    return addInteraction(userNode, 1L);
	}
	
	public InteractsRelation addInteraction(UserNode userNode, Long weight){
	    InteractsRelation interaction = getInteractionWithUser(userNode);
        if(interaction==null){
            return addInteractingUser(userNode, weight);
        }
        interaction.weight += weight;
        return interaction;
    }
	
	private InteractsRelation getInteractionWithUser(UserNode userNode){
	    if(users==null){
	        return null;
	    }
	    for (InteractsRelation interaction : users) {
	        if (interaction.user2.nodeId.equals(userNode.nodeId))
	            return interaction;
	    }
	    return null;
	}
	
	public Set<InteractsRelation> getInteractingUsers(){
		return users;
	}
	
	
	public InterestedRelation addInterestedRelation(TopicNode topicNode){	
		return addInterestedRelation(topicNode, 1L);
	}
	
	public InterestedRelation addInterestedRelation(TopicNode topicNode, Long weight){
		InterestedRelation interested=this.getInterestedInTopicRelation(topicNode);

		if(interested==null){			
			return addInterestedTopic(topicNode,weight);
		}

		interested.weight+=weight;
		
		return interested;
	}
	
	private InterestedRelation getInterestedInTopicRelation(TopicNode topicNode){
		if(topics==null){
			return null;
	    }
		
	    for (InterestedRelation interested : topics) {
	        if (interested.topic.nodeId.equals(topicNode.nodeId))
	            return interested;
	    }
	    
	    return null;
	}
	
	
	public InterestedRelation addInterestedTopic(TopicNode topicNode, long weight){
		if(topics==null){
			topics=new HashSet<InterestedRelation>();
		}
		
		InterestedRelation interested=new InterestedRelation(this,topicNode,weight);
		topics.add(interested);
		
		return interested;
	}
	
	public Set<InterestedRelation> getInterestedInTopics(){
		return topics;
	}
	
	@Override
	public String toString() {
	    return String.format("UserNode<id: %d, user: %d>", nodeId, userId);
	}
	
}
