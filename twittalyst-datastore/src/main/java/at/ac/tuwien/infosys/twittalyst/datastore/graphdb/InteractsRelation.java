package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type="INTERACTS_WITH")
public class InteractsRelation {
	@GraphId Long relationId;
	@StartNode public UserNode user1;
	@EndNode public UserNode user2;
	public Long weight;
	
	public InteractsRelation(){};
	
	public InteractsRelation(UserNode user1, UserNode user2, Long weight){
		this.user1=user1;
		this.user2=user2;
		this.weight=weight;
	}
	
	@Override
	public String toString() {
	    return String.format("Interaction %s -> %s, weight %d",  user1, user2, weight) ;
	}
}
