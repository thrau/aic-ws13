package at.ac.tuwien.infosys.twittalyst.datastore.graphdb;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.neo4j.cypher.SyntaxException;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CypherREPL {

    private static Logger logger = LoggerFactory.getLogger(CypherREPL.class);
    
    public static void main(String[] args) throws SyntaxException, IOException {
        String dbPath = "data/main/graph.db";
        
        if (args.length == 1) {
            dbPath = args[0];
        }
        
        if (!new File(dbPath).isDirectory()) {
            logger.error("Directory \"" + dbPath + "\" does not exist!");
            return;
        }
        
        logger.info("Loading Graph DB...");
        GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabase(dbPath);
        logger.info("Graph DB loaded");
        
        
        ExecutionEngine engine = new ExecutionEngine(db);        
        BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        
        logger.info("Interactive Cypher REPL!");
        System.out.print("> ");
        
        while ((line = buffer.readLine()) != null) {
            if (!line.isEmpty()) {
                try {
                    ExecutionResult res = engine.execute(line);
                    System.out.println(res.dumpToString());
                } catch(Exception e) {
                    logger.error("An error occurred", e);
                }
            }
            System.out.print("> ");
        }
        
        System.out.println("Goodbye");
        db.shutdown();
    }

}
