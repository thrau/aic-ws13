package at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories;

import org.springframework.data.neo4j.repository.GraphRepository;

import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.InterestedRelation;

public interface InterestedRelationRepository extends GraphRepository<InterestedRelation> {
    
}
