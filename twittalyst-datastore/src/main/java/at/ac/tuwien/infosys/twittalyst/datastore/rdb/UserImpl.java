package at.ac.tuwien.infosys.twittalyst.datastore.rdb;

public class UserImpl implements User {

    private long id;
    private String name;
    private Long followerCount=-1L;
    private Long friendCount=-1L;
    private Long tweetCount=-1L;
    
    public UserImpl(long id, String name, Long followerCount, Long friendCount, Long tweetCount) {
        this.id = id;
        this.name = name;
        this.followerCount = followerCount;
        this.friendCount = friendCount;
        this.tweetCount = tweetCount;
    }
    
    public UserImpl(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    @Override
    public long getID() {
        return id;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
	@Override
	public void setName(String name) {
		this.name=name;
	}
	
    @Override
    public Long getFollowerCount() {
        return followerCount;
    }
    
    @Override
    public Long getFriendCount() {
        return friendCount;
    }
    
    @Override
    public Long getTweetCount() {
        return tweetCount;
    }
    
    @Override
    public String toString() {
        return String.format("{@%s: %d followers, %d friends, %d tweets, id %d}", name, followerCount, friendCount, tweetCount, id);
    }

	@Override
	public void setFollowerCount(Long followerCount) {
		this.followerCount=followerCount;
		
	}

	@Override
	public void setFriendCount(Long friendCount) {
		this.friendCount=friendCount;
	}

	@Override
	public void setTweetCount(Long tweetCount) {
		this.tweetCount=tweetCount;
	} 
}
