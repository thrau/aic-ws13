package at.ac.tuwien.infosys.twittalyst.analyzer.dump;

import java.io.Closeable;
import java.io.IOException;

import at.ac.tuwien.infosys.twittalyst.analyzer.TweetParser;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;

public interface StreamDumpReader extends Closeable, AutoCloseable {

    /**
     * Reads the next line from the dump file and converts it to a {@link Tweet} using the {@link TweetParser}.
     * 
     * @return the next {@link Tweet} in the dump or <code>null</code> if the end of the file has been reached.
     * @throws IOException if an I/O error occurs.
     */
    Tweet readTweet() throws IOException;
}
