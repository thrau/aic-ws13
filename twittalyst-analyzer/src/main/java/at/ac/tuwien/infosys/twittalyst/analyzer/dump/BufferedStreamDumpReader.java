package at.ac.tuwien.infosys.twittalyst.analyzer.dump;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.analyzer.TweetParser;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;

/**
 * Concurrent version of a {@link StreamDumpReader}. Uses two worker threads to read from the dump file, and parse the
 * contents using the {@link TweetParser}. For this, two queues are used. The file reader worker writes lines to the
 * {@code readBuffer}, which are consumed by the parser worker thread, which then writes parsed {@link Tweet} objects
 * into the {@code parseBuffer}. The elements in the {@code parseBuffer} are consumed when calling {@link #readTweet()}.
 * <p>
 * This has an increased performance when reading large files (&gt; 1GB).
 */
public class BufferedStreamDumpReader implements StreamDumpReader {

    private static final Logger LOG = LoggerFactory.getLogger(BufferedStreamDumpReader.class);

    private static final int DEFAULT_BUFFER_SIZE = 256;

    private static final Tweet DONE = new Tweet(); // marker object for parser queue

    private StreamDump dump;

    private BlockingQueue<String> readBuffer;
    private BlockingQueue<Tweet> parseBuffer;

    private ReaderWorker readerWorker;
    private ParserWorker parserWorker;

    private boolean started;
    private boolean eof;
    private boolean closed;

    /**
     * Creates a new {@link StreamDumpReader} with the default buffer size.
     * 
     * @param dump the {@link StreamDump} to read
     */
    public BufferedStreamDumpReader(StreamDump dump) {
        this(dump, DEFAULT_BUFFER_SIZE);
    }

    /**
     * Creates a new {@link StreamDumpReader} with the given buffer size.
     * 
     * @param dump the {@link StreamDump} to read
     * @param bufferSize the size of the queues
     */
    public BufferedStreamDumpReader(StreamDump dump, int bufferSize) {
        if (bufferSize <= 0) {
            throw new IllegalArgumentException("bufferSize <= 0");
        }
        this.dump = dump;
        this.readBuffer = new ArrayBlockingQueue<>(bufferSize);
        this.parseBuffer = new ArrayBlockingQueue<>(bufferSize);
    }

    @Override
    public Tweet readTweet() throws IOException {
        if (!started) {
            startWorkers();
        }

        while (!eof) {
            try {
                return parseBuffer.take();
            } catch (InterruptedException e) {
            }
        }

        while (true) {
            try {
                Tweet poll = parseBuffer.take();

                if (poll == DONE) {
                    LOG.debug("Done marker received.");
                    return null;
                } else {
                    return poll;
                }
            } catch (InterruptedException e) {
            }
        }
    }

    private void startWorkers() {
        started = true;

        readerWorker = new ReaderWorker();
        parserWorker = new ParserWorker();

        LOG.debug("Starting BufferedStreamDumpReader worker threads");

        readerWorker.start();
        parserWorker.start();
    }

    @Override
    public synchronized void close() throws IOException {
        LOG.info("Closing BufferedStreamDumpReader");
        closed = true;
        readerWorker.close();
        parserWorker.close();
    }

    /**
     * Worker thread that reads the dump file with a {@link BufferedReader}.
     */
    private class ReaderWorker extends Thread implements Closeable {

        private BufferedReader reader;

        public ReaderWorker() {
            LOG.debug("Creating new ReaderWorker");
            try {
                reader = new BufferedReader(new FileReader(dump.getDumpFile()));
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void run() {
            LOG.debug("Running ReaderWorker {}", getName());

            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    readBuffer.put(line);
                }
                LOG.debug("EOF reached {}", getName());
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                LOG.debug("{} interrupted", getName());
            } finally {
                synchronized (this) {
                    eof = true;
                }
            }

        }

        @Override
        public void close() throws IOException {
            LOG.debug("Closing ReaderWorker {}", getName());
            interrupt();
            reader.close();
        }

    }

    /**
     * Worker thread that parses the strings to {@link Tweet} objects.
     * 
     */
    private class ParserWorker extends Thread implements Closeable {
        private TweetParser parser;

        public ParserWorker() {
            parser = new TweetParser();
        }

        @Override
        public void run() {
            LOG.debug("Running ParserWorker {}", getName());

            // take while reading
            while (!eof) {
                String take;

                try {
                    take = readBuffer.take();
                } catch (InterruptedException e) {
                    LOG.debug("{} interrupted while taking element", getName());
                    continue;
                }

                put(take);
            }

            LOG.debug("ParserWorker {} polling", getName());
            while (!readBuffer.isEmpty()) {
                put(readBuffer.poll());
            }

            LOG.debug("ParserWorker {} done", getName());
            put(DONE);
        }

        private void put(String line) {
            try {
                put(parser.fromTwitterStream(line));
            } catch (IOException e) {
                throw new RuntimeException("Exception while parsing string", e);
            }
        }

        private void put(final Tweet t) {
            while (t != null && !closed) {
                try {
                    parseBuffer.put(t);
                    break;
                } catch (InterruptedException e) {
                    LOG.debug("{} interrupted while putting element", getName());
                    continue;
                }
            }
        }

        @Override
        public void close() throws IOException {
            LOG.debug("Closing ParserWorker {}", getName());
            eof = true;
            interrupt();
        }
    }

}
