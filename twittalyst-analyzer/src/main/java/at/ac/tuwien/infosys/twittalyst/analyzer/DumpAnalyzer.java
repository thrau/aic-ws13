package at.ac.tuwien.infosys.twittalyst.analyzer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDumpReader;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetUser;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.RelationsDAO;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.UserRepository;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.UserDAO;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.UserImpl;

public class DumpAnalyzer {
    
    private Logger logger = LoggerFactory.getLogger(DumpAnalyzer.class);
	
    @Autowired
    private UserDAO userDAO;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private TopicDestilator topicDestilator;
    
    @Autowired
    private RelationsDAO relationsDAO;
    
    private boolean finishing = false;
    
    private HashSet<Long> interactionIds = new HashSet<>();
    private ArrayList<User> interactionUsers = new ArrayList<>();
    
    public void analyzeDump(StreamDumpReader streamDumpReader) throws IOException {
    	Tweet tweet;
        while (!finishing && (tweet = streamDumpReader.readTweet()) != null) {
        	logger.debug(String.format("Found tweet of user %s.",tweet.getAuthor().getScreenName()));
        	processTweet(tweet);
        }
    }
    
    /**
     * Extracts information from a tweet and saves it to the databases.
     * @param tweet Tweet to analyze.
     */
    public void processTweet(Tweet tweet) {
        processUser(tweet.getAuthor());
        
        relationsDAO.addUserWithInteractionsAndInterests(
            tweet.getAuthor().getId(), 
            processTweetInteractions(tweet),
            processTopics(tweet));
    }
    
    //saves the users appearing in tweet (replied, retweeted user and mentioned users)
    //relationship weight is only increased by 1 even if user is mentioned and retweeted/replied
    private Set<Long> processTweetInteractions(Tweet tweet) {
        interactionIds.clear();
        interactionUsers.clear();
        
        for (TweetUser user : tweet.getMentions()) {
            interactionIds.add(user.getId());
            processUser(user);
        }
        
        TweetUser user = tweet.getRefferedToUser();
        if (user != null) {
            interactionIds.add(user.getId());
            processUser(user);
        }
        
        userDAO.batchUpsert(interactionUsers);
        return interactionIds;
    }
    
    private void processHashtags(Tweet tweet) {
        if (tweet.getHashtags() == null) return;
        // TODO save hashtags if needed
    }
    
    private List<String> processTopics(Tweet tweet) {
        List<String> topics = topicDestilator.destilate(tweet);
        
        logger.debug("@" + tweet.getAuthor().getScreenName() + " is interested in " + topics);
        if (topics.size() == 0) return null;
        
        return topics;
    }
    
    /**
     * Extracts information about a user and stores her in the relational database.
     * @param tweetUser The user to analyze.
     */
    private void processUser(TweetUser tweetUser) {
        long id = tweetUser.getId();
        String name = tweetUser.getScreenName();
        Long followerCount = tweetUser.getFollowers();
        Long friendCount = tweetUser.getFriends();
        Long tweetCount = tweetUser.getTweets();
        
        User user = new UserImpl(id, name, followerCount, friendCount, tweetCount);
        userDAO.upsert(user);
    }
    
    
    
    public void shutdown() {
        finishing = true;
    }
}
