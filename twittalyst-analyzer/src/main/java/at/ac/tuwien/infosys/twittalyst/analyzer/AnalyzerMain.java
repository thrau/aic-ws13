package at.ac.tuwien.infosys.twittalyst.analyzer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.neo4j.graphdb.GraphDatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import at.ac.tuwien.infosys.twittalyst.analyzer.dump.SimpleStreamDumpReader;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDump;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDumpReader;


public class AnalyzerMain {
	
	private static Logger logger = LoggerFactory.getLogger(AnalyzerMain.class);
	
	private static String[] usedSpringContexts = {"ac-analyzer.xml","ac-datastore.xml"};
	
	private static final String usage = " Usage: Analyzer <pathToDump> [<tweetsToSkip>]";
	
	private static DumpAnalyzer reader;

	public static void main(String[] args) {
		if(args.length < 1) {
			logger.error("Missing Tweet-Dump Path.\n" + usage);
			return;
		}
		File dumpFile = new File(args[0]);
		if(!dumpFile.isFile()){
		    logger.error("Supplied Path must point to Tweet-Dump.\n" + usage);
		    return;
		}
		int skip = 0;
		if (args.length >= 2) {
		    try {
		        skip = Integer.valueOf(args[1]);
		    } catch (NumberFormatException e) {
		        logger.error("Invalid number of Tweets to skip specified.\n" + usage);
		    }
		}
		
		final StreamDumpReader streamDumpReader;
		try {
			streamDumpReader = new SimpleStreamDumpReader(new StreamDump(dumpFile, null, null), skip);
		} catch (FileNotFoundException e) {
			logger.error("Supplied Path must point to Tweet-Dump.\n" + usage, e);
			return;
		} catch (IOException e) {
		    logger.error("Error reading Tweet-Dump.", e);
            return;
		}
		
		/*Init Spring*/
		final ApplicationContext applicationContext = new ClassPathXmlApplicationContext(usedSpringContexts);
		reader = (DumpAnalyzer) applicationContext.getBean("DumpAnalyzer");
		
		final Thread analyzerThread = new Thread() {
		    public void run() {
		        try {
		            reader.analyzeDump(streamDumpReader);
		        } catch (IOException e) {
		            logger.error("Error while prozessing dump file.", e);
		            return;
		        } finally {
		            try {
		                streamDumpReader.close();
		            } catch (IOException e) {
		                logger.error("Error closing StreamDumpReader.", e);
		            }
		        }
		        logger.info("Finished analyzing");
		    }
		};
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
		    public void run() {
		        logger.info("Shutting down...");
		        try {
		            reader.shutdown();
		            analyzerThread.join();
		            
		            // shut down neo4j db cleanly
		            logger.info("Shutting down neo4j database..");
		            GraphDatabaseService db = applicationContext.getBean(GraphDatabaseService.class);
		            db.shutdown();
		        } catch (InterruptedException e) {
		            logger.info("Join interrupted..");
		        }
		        
		        logger.info("Goodbye");
		        
		    }
		});
		
		/*Start Analyzing*/
		analyzerThread.setName("AnalyzerThread");
		analyzerThread.start();
	}
	

}
