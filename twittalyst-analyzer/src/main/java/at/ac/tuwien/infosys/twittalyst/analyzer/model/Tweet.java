package at.ac.tuwien.infosys.twittalyst.analyzer.model;

import java.io.Serializable;
import java.text.MessageFormat;

public class Tweet implements Serializable {

    private static final long serialVersionUID = -810838123178184411L;

    private String text= "";
    private TweetUser author = null;
    private TweetType type = TweetType.TWEET;
    private TweetUser[] mentions={};
    private TweetUser refferedToUser = null;
    
    private String[] hashtags = {};

    public String[] getHashtags() {
        return hashtags;
    }

    public void setHashtags(String[] hashtags) {
        this.hashtags = hashtags;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TweetUser getAuthor() {
        return author;
    }

    public void setAuthor(TweetUser author) {
        this.author = author;
    }

    public TweetType getType() {
        return type;
    }

    public void setType(TweetType type) {
        this.type = type;
    }

    public TweetUser[] getMentions() {
        return mentions;
    }

    public void setMentions(TweetUser[] mentions) {
        this.mentions = mentions;
    }

    public TweetUser getRefferedToUser() {
        return refferedToUser;
    }

    public void setRefferedToUser(TweetUser refferedToUser) {
        this.refferedToUser = refferedToUser;
    }

    public String toString(){
        String s;
        String out;
        
        String tags= "";
        for (String j: hashtags){
            tags += j;
            tags += ", ";
        }
        
        String mentioned="\n";
        for (TweetUser u : mentions){
            mentioned += u.toString();
            mentioned += "\n";
        }
        
        
        s = "Text: {0}\nAuthor:\n\n{1}\nMentions: {2}\nHashtags: {3}\nType: {4}\nReffers to: {5})";
        out = MessageFormat.format(s, text, author, mentioned, tags,type, (refferedToUser == null) ? "-" : refferedToUser.getScreenName());
        
        return out;
    }

}
