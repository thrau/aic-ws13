package at.ac.tuwien.infosys.twittalyst.analyzer;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;
import at.ac.tuwien.infosys.twittalyst.datastore.docdb.AdvertisementDAO;

public class TopicDestilator {
    
    @Autowired
    private AdvertisementDAO dao; 
    
    private Map<String, ? extends Set<String>> topics;
    
    public TopicDestilator(){}
    
    public List<String> destilate(Tweet t){
        String text = t.getText().toLowerCase();
        //String[] no_stop = text.split(" ").removeStopwords(text);
        String [] no_special = removeSpecials(text).split(" ");
        LinkedList<String> tweetTopics = findTopicsForWords(no_special);
        return tweetTopics;
    }

    private LinkedList<String> findTopicsForWords(String[] no_stop) {
        if (topics == null) topics = dao.getTopics();
        LinkedList<String> tweettopics = new LinkedList<String>();
        
        for(String word : no_stop){
            LinkedList<String> tmp = findTopicsForWord(word, topics);
            tweettopics.addAll(tmp);
        }

        return tweettopics;
    }

    private LinkedList<String> findTopicsForWord(String word, Map<String, ? extends Set<String>> topics) {
        LinkedList<String> tmp = new LinkedList<String>();
        for(Entry<String, ? extends Set<String>> entry: topics.entrySet()){
            if(entry.getValue().contains(word)){
                tmp.add(entry.getKey());
            }
        }
        return tmp;
    }

    private String[] removeStopwords(String[] text) {
        // TODO implement iff performance is down the drain
        return text;
    }

    private String removeSpecials(String text) {
        return text.replaceAll("[!?:;.,]", "");
    }

}
