package at.ac.tuwien.infosys.twittalyst.analyzer.dump;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.analyzer.TweetUserSet;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetUser;
import at.ac.tuwien.infosys.twittalyst.common.util.Predicate;

/**
 * Reads a given {@link StreamDump} and encapsulates some analysis methods on the stream.
 */
public class StreamDumpAnalyzer {

    private static final Logger LOG = LoggerFactory.getLogger(StreamDumpAnalyzer.class);

    private static final Predicate<Tweet> VERIFIED_AUTHOR_PREDICATE = new Predicate<Tweet>() {

        @Override
        public boolean evaluate(Tweet element) {
            return element.getAuthor().isVerified();
        }

    };

    private StreamDump dump;

    public StreamDumpAnalyzer(StreamDump dump) {
        this.dump = dump;
    }

    /**
     * Iteratively scans the {@link StreamDump} for interesting users to follow. The root iteration pulls out every
     * {@link TweetUser} with the 'verified' flag set to true, and each iteration adds all users that were mentioned by
     * users gathered in the previous iteration. The process will break if no new users were added during an interation.
     * <p>
     * This helps to builds a robust and closed set of 'trusted' users that can be used to filter the public stream by.
     * 
     * @param depth the depth of the iteration. Each iteration step parser the entire file!
     * @param limit limit the amount of users added
     * @param users the tweet user set to add users into
     * 
     * @return a {@link TweetUserSet} of users to follow.
     */
    public void whoToFollow(int depth, int limit, final TweetUserSet users) {
        whoToFollow(limit, users, VERIFIED_AUTHOR_PREDICATE); // bootstrap

        LOG.debug("starting iterations with {} verified users", users.size());

        for (int i = 0; i <= depth; i++) {
            int len = users.size();

            whoToFollow((limit / 3), users, new Predicate<Tweet>() {

                @Override
                public boolean evaluate(Tweet element) {
                    return users.contains(element.getAuthor());
                }
            });

            if (users.size() >= limit) {
                LOG.debug("reached the user limit with {}", users.size());
                return;
            }
            if (len == users.size()) {
                LOG.debug("no new users gathered during iteration {}, breaking", i + 1);
                return;
            }

            LOG.debug("finishing iteration {} with {} users", i + 1, users.size());
        }
    }

    /**
     * Iteratively scans the {@link StreamDump} for interesting users to follow. The root iteration pulls out every
     * {@link TweetUser} with the 'verified' flag set to true, and each iteration adds all users that were mentioned by
     * users gathered in the previous iteration. The process will break if no new users were added during an interation.
     * <p>
     * This helps to builds a robust and closed set of 'trusted' users that can be used to filter the public stream by.
     * 
     * @param depth the depth of the iteration. Each iteration step parser the entire file!
     * 
     * @return a {@link TweetUserSet} of users to follow.
     */
    public TweetUserSet whoToFollow(int depth, int limit) {
        final TweetUserSet users = new TweetUserSet();

        whoToFollow(limit, users, VERIFIED_AUTHOR_PREDICATE); // bootstrap

        LOG.debug("starting iterations with {} verified users", users.size());

        for (int i = 0; i <= depth; i++) {
            int len = users.size();

            whoToFollow(limit, users, new Predicate<Tweet>() {

                @Override
                public boolean evaluate(Tweet element) {
                    return users.contains(element.getAuthor());
                }
            });

            if (len == users.size()) {
                LOG.debug("no new users gathered during iteration {}, breaking", i + 1);
                return users;
            }

            LOG.debug("finishing iteration {} with {} users", i + 1, users.size());
        }

        return users;
    }

    private void whoToFollow(int limit, TweetUserSet users, Predicate<Tweet> predicate) {
        try (StreamDumpReader reader = new BufferedStreamDumpReader(dump)) {
            LOG.debug("reading dump {} ...", dump.getId());

            Tweet t;
            while ((t = reader.readTweet()) != null) {
                if (predicate.evaluate(t)) {
                    for (TweetUser mention : t.getMentions()) {
                        LOG.debug("following {}", mention.getScreenName());
                        users.add(mention);
                    }

                    LOG.debug("following {}", t.getAuthor().getScreenName());
                    users.add(t.getAuthor());
                }

                if (users.size() >= limit) {
                    return;
                }
            }
        } catch (IOException e) {
            LOG.error("IO error while reading stream dump", e);
            throw new RuntimeException(e);
        }
    }

}
