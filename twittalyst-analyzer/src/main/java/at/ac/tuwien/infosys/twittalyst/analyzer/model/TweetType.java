package at.ac.tuwien.infosys.twittalyst.analyzer.model;

public enum TweetType {
    RETWEET,
    REPLY,
    TWEET
}
