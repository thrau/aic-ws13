package at.ac.tuwien.infosys.twittalyst.analyzer.dump;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.common.util.Utils;

/**
 * Represents the logged output of a stream recording session.
 */
public final class StreamDump {

    private static final Logger LOG = LoggerFactory.getLogger(StreamDump.class);

    public static final String DUMP_FILE_PREFIX = "twittalyst.dump.";
    public static final String FOLLOW_FILE_PREFIX = "twittalyst.follow.";

    public static final String GLOB = "twittalyst.{dump,follow}.%s";
    public static final String REGEX = "twittalyst\\.(dump|follow)\\.([a-z0-9]+)";

    public static final Pattern REGEX_PATTERN = Pattern.compile(REGEX);

    private final String id;
    private final File dump;
    private final File follow;

    /**
     * Creates a new {@link StreamDump} with a random 7 digit hex id in the given directory.
     * 
     * @param dir the directory to create the stream dump in.
     */
    public StreamDump(Path dir) {
        this(dir, Utils.randomId(7));
    }

    /**
     * Creates a new {@link StreamDump} in the given directory with the given id. Use this constructor to 'revive' old
     * stream dumps.
     * 
     * @param dir the directory to read the stream dump from.
     * @param id
     */
    public StreamDump(Path dir, String id) {
        this.dump = new File(dir.resolve(DUMP_FILE_PREFIX + id).toString());
        this.follow = new File(dir.resolve(FOLLOW_FILE_PREFIX + id).toString());
        this.id = id;
    }
    
    public StreamDump(File dump, File follow, String id) {
        this.dump = dump;
        this.follow = follow;
        this.id = id;
    }

    /**
     * Returns the dump file. This is the file that contains the tweets as raw stream data.
     * 
     * @return the dump file.
     */
    public File getDumpFile() {
        return dump;
    }

    /**
     * Returns the follow file. This is the file that contains the list of users that were followed during the stream
     * recording.
     * 
     * @return the follow file.
     */
    public File getFollowFile() {
        return follow;
    }

    /**
     * Returns a new {@link FileInputStream} of the dump file.
     * 
     * @return a new {@link FileInputStream}
     */
    public FileInputStream getDumpInputStream() {
        try {
            return new FileInputStream(getDumpFile());
        } catch (FileNotFoundException e) {
            LOG.error("Dump file {} not found", getDumpFile().getName(), e);
            return null;
        }
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("dump", dump).append("follow", follow).toString();
    }

}
