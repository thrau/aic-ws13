package at.ac.tuwien.infosys.twittalyst.analyzer.model;

import java.io.Serializable;
import java.text.MessageFormat;

import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;

public class TweetUser implements Serializable {

    private static final long serialVersionUID = -8489863728303348918L;

    private long id;
    private String screenName;
    private Long followers = -1L;
    private Long friends = -1L;
    private Long tweets = -1L;
    private boolean verified;

    public TweetUser(){}
    
    public TweetUser(User user){
    	this.id=user.getID();
    	this.followers=user.getFollowerCount();
    	this.friends=user.getFriendCount();
    	this.screenName=user.getName();
    	this.tweets=user.getTweetCount();
    }
    
    public String toString() {
        String s = "ID: {0}\nScreenName: {1}\nFollowers: {2}\nFriends: {3}\nTweets: {4}\n";
        String out = MessageFormat.format(s, id, screenName, followers, friends, tweets);
        return out;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public Long getFollowers() {
        return followers;
    }

    public void setFollowers(long followers) {
        this.followers = followers;
    }

    public Long getFriends() {
        return friends;
    }

    public void setFriends(long friends) {
        this.friends = friends;
    }

    public Long getTweets() {
        return tweets;
    }

    public void setTweets(long tweets) {
        this.tweets = tweets;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

}
