package at.ac.tuwien.infosys.twittalyst.analyzer.dump;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used while recording twitter stream data into a {@link StreamDump}.
 */
public class StreamDumpWriter {

    private static final Logger LOG = LoggerFactory.getLogger(StreamDumpWriter.class);

    private static final int DEFAULT_BUFFER_SIZE = 16384;

    private StreamDump dump;

    /**
     * Create a new {@link StreamDumpWriter} on the given {@link StreamDump}. Will create files if they do not exist.
     * 
     * @param dump the dump to write to
     */
    public StreamDumpWriter(StreamDump dump) {
        this.dump = dump;
    }

    /**
     * Writes the given array of user ids into the follow file of this dump record.
     * 
     * @param userIds the list of user ids.
     * @throws IOException if an IO error occurred while accessing the file
     */
    public void writeFollowList(long[] userIds) throws IOException {
        try (FileWriter fr = new FileWriter(dump.getFollowFile(), false)) {
            fr.write(Arrays.toString(userIds));
            fr.flush();
        }
    }

    /**
     * Appends the given strings to the dump file.
     * 
     * @param messages the stream messages to write
     * @throws IOException on IO errors
     */
    public void write(List<String> messages) throws IOException {
        try (BufferedWriter writer = newDumpFileWriter()) {
            LOG.trace("writing {} messages ...", messages.size());
            for (String message : messages) {
                writer.write(message + "\n");
            }
            writer.flush();
        } finally {
            LOG.trace("... done writing messages!");
        }
    }

    /**
     * Runs the {@link #write(List)} operation in a new task executed by the given {@link ExecutorService}.
     * 
     * @param messages the messages to write
     * @param executor the service that will execute the task
     */
    public void write(final List<String> messages, ExecutorService executor) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    write(messages);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public StreamDump getDump() {
        return dump;
    }

    private BufferedWriter newDumpFileWriter() throws IOException {
        return new BufferedWriter(new FileWriter(dump.getDumpFile(), true), DEFAULT_BUFFER_SIZE);
    }

}
