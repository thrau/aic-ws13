package at.ac.tuwien.infosys.twittalyst.analyzer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetUser;

/**
 * A special set made to manage users by their id rather than object equality. If the same users are added twice, it
 * will update users that do not have enriched information (i.e. were added via the Mention entity).
 * <p>
 * The contains and remove functions can be used with Long (representing user ids) and {@link TweetUser} objects alike.
 */
public class TweetUserSet implements Set<TweetUser> {
    private Map<Long, TweetUser> map;

    public TweetUserSet() {
        this(new HashMap<Long, TweetUser>());
    }

    public TweetUserSet(int initialCapacity) {
        this(new HashMap<Long, TweetUser>(initialCapacity));
    }

    protected TweetUserSet(Map<Long, TweetUser> map) {
        this.map = map;
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof TweetUser) {
            return contains((TweetUser) o);
        } else if (o instanceof Long) {
            return contains((Long) o);
        } else {
            return map.containsValue(o);
        }
    }

    public boolean contains(TweetUser user) {
        return map.containsKey(user.getId());
    }

    public boolean contains(Long id) {
        return map.containsKey(id);
    }

    @Override
    public Iterator<TweetUser> iterator() {
        return map.values().iterator();
    }

    @Override
    public Object[] toArray() {
        return map.values().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return map.values().toArray(a);
    }

    @Override
    public boolean add(TweetUser e) {

        TweetUser user = map.get(e.getId());
        if (user != null) {
            if (isEnriched(user)) {
                return false;
            } else {
                map.put(e.getId(), e);
                return true;
            }
        }

        return map.put(e.getId(), e) == null;
    }

    private boolean isEnriched(TweetUser user) {
        return user.getTweets() != -1;
    }

    @Override
    public boolean remove(Object o) {
        if (o instanceof TweetUser) {
            return map.remove(((TweetUser) o).getId()) != null;
        } else {
            return map.remove(o) != null;
        }

    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends TweetUser> c) {
        boolean add = false;
        for (TweetUser tweetUser : c) {
            if (add(tweetUser)) {
                add = true;
            }
        }
        return add;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object object : c) {
            if (object instanceof TweetUser) {
                return map.remove(object) != null;
            }
        }

        return false;
    }

    @Override
    public void clear() {
        map.clear();
    }

    public long[] getUserIdArray() {
        long[] ids = new long[map.size()];

        int i = 0;
        for (Long id : map.keySet()) {
            ids[i++] = id;
        }

        return ids;
    }

}
