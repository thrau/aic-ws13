package at.ac.tuwien.infosys.twittalyst.analyzer.dump;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.analyzer.TweetParser;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;
import at.ac.tuwien.infosys.twittalyst.common.util.Collections;
import at.ac.tuwien.infosys.twittalyst.common.util.Utils;

/**
 * Reads {@link Tweet} objects from a given {@link StreamDump}.
 */
public class SimpleStreamDumpReader implements StreamDumpReader {
    private static Logger logger = LoggerFactory.getLogger(SimpleStreamDumpReader.class);
    
    private StreamDump dump;

    private TweetParser parser;
    private BufferedReader dumpFileReader;

    private long[] followList;
    private long currentTweet = 0;

    public SimpleStreamDumpReader(StreamDump dump, int skip) throws IOException {
        this.dump = dump;

        dumpFileReader = new BufferedReader(new FileReader(dump.getDumpFile()));
        
        for (int i = 0; i < skip; i++) {
            dumpFileReader.readLine();
            currentTweet++;
        }
        
        parser = new TweetParser();
    }
    
    public SimpleStreamDumpReader(StreamDump dump) throws IOException {
        this(dump, 0);
    }

    /**
     * Returns the list of users that the stream was filtered by while recording the dump.
     * 
     * @return a list of twitter user ids.
     */
    public long[] getFollowList() {
        if (followList == null) {
            followList = Collections.parseLongArray(Utils.readFile(dump.getFollowFile()));
        }

        return followList;
    }

    @Override
    public Tweet readTweet() throws IOException {
        String line = dumpFileReader.readLine();
        currentTweet++;
        
        if(currentTweet%1000==0){
        	logger.info("Reading tweet #"+currentTweet);
        }
        
        
        if (line == null) {
            return null;
        }

        return parser.fromTwitterStream(line);
    }

    @Override
    public void close() throws IOException {
        logger.info("Closing at tweet #" + currentTweet);
        
        if (dumpFileReader != null) {
            dumpFileReader.close();
        }
    }

    /**
     * Returns the {@link StreamDump} the current reader instance is reading.
     * 
     * @return the stream dump.
     */
    public StreamDump getDump() {
        return dump;
    }

}
