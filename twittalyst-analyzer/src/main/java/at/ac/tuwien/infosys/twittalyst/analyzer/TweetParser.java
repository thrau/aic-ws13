package at.ac.tuwien.infosys.twittalyst.analyzer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetType;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetUser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

public class TweetParser {
    
    private Logger logger = LoggerFactory.getLogger(TweetParser.class);
    
    @Value("${outpath}")
    private String outPath;
    
    private GsonBuilder gsonBuilder = new GsonBuilder();
    private Gson gson;
    

    public TweetParser(){
        logger.trace("New TweetParser");
        gson = gsonBuilder.create();
    }
    
    public Tweet fromJSON(String s){
        logger.trace("Reading Tweet from JSON");
        return gson.fromJson(s,Tweet.class);
    }
    
    public String toJSON(Tweet t){
        logger.trace("Converting Tweet to JSON");
        return gson.toJson(t);
    }
    
    public void fromTwitterStream(String[] tweets) throws IOException{
        logger.trace("Reading Tweets from Stream");
        
        File f = new File(outPath);
        FileWriter fw = new FileWriter(f.getAbsolutePath());
        
        for(String s: tweets){
            Tweet t = fromTwitterStream(s);
            gson.toJson(t, fw);
        }
        
        fw.close();
    }
    
    public Tweet fromTwitterStream(String s) throws IOException{
        logger.trace("Reading single Tweet from Stream");
        
        JsonReader reader = new JsonReader(new StringReader(s));

        return readTweetFromString(reader);
    }

    private Tweet readTweetFromString(JsonReader reader) throws IOException {
        logger.trace("Reading Tweet from String");
        Tweet t = new Tweet();
        
        reader.beginObject();
        
        while(reader.hasNext()){
            String name = reader.nextName();
            
            if(name.equals("text")){
                t.setText(reader.nextString());
            }
            else if(name.equals("user")){
                t.setAuthor(readUser(reader));                
            }
            else if(name.equals("entities")){
                Entity e = readEntity(reader);
                t.setHashtags(e.getHashtags());
                t.setMentions(e.getMentions());
            }
            else if(name.equals("retweeted_status")){
                TweetUser originalUser = getOriginalTweetUser(reader);
                if(originalUser != null){
                    logger.trace("Is Retweet.");
                    t.setType(TweetType.RETWEET);
                    t.setRefferedToUser(originalUser);
                }
            }
            else if(name.equals("in_reply_to_user_id")){
                try{
                    long originalUID = reader.nextLong();
                    if (t.getRefferedToUser() == null) t.setRefferedToUser(new TweetUser());
                    t.getRefferedToUser().setId(originalUID);
                }
                catch(IllegalStateException e){
                    reader.skipValue();
                    continue;
                }
                logger.trace("Is Reply.");
                t.setType(TweetType.REPLY);
                
            }
            else if(name.equals("in_reply_to_screen_name")){
                
                if (reader.peek() == JsonToken.STRING) {
                    if (t.getRefferedToUser() == null) t.setRefferedToUser(new TweetUser());
                    t.getRefferedToUser().setScreenName(reader.nextString());
                    logger.trace("Is Reply.");
                    t.setType(TweetType.REPLY);
                } else {
                    reader.skipValue();
                }
            }
            else{
                reader.skipValue();
            }
            
        }
        
        reader.endObject();
        return t;
    }

    private TweetUser getOriginalTweetUser(JsonReader reader) throws IOException {
        logger.trace("Reading UID from original Tweet");
        TweetUser user = null;
        reader.beginObject();
        while(reader.hasNext()){
            String name = reader.nextName();
            if(name.equals("user")){
                user = readUser(reader);
            }
            else{
                reader.skipValue();
            }
        }
        reader.endObject();
        return user;
    }

    private TweetUser readUser(JsonReader reader) throws IOException {
        logger.trace("Reading User.");
        TweetUser u = new TweetUser();
        reader.beginObject();
        
        while(reader.hasNext()){
            String name = reader.nextName();
            
            if(name.equals("screen_name")){
                u.setScreenName(reader.nextString());
            }
            else if(name.equals("id")){
                u.setId(reader.nextLong());
            }
            else if(name.equals("statuses_count")){
                u.setTweets(reader.nextLong());
            }
            else if(name.equals("followers_count")){
                u.setFollowers(reader.nextLong());
            }
            else if(name.equals("friends_count")){
                u.setFriends(reader.nextLong());
            }
            else if(name.equals("verified")) {
                u.setVerified(reader.nextBoolean());
            }
            else{
                reader.skipValue();
            }
        }
        
        reader.endObject();
        return u;
    }

    private Entity readEntity(JsonReader reader) throws IOException {
        logger.trace("Reading Entity");
        Entity e = new Entity();
        reader.beginObject();
        
        while(reader.hasNext()){
            String name = reader.nextName();
            
            if (name.equals("hashtags")){
                // TODO read hashtags if needed?
                // e.setHashtags(readHashtags(reader));
                reader.skipValue();
            }
            else if(name.equals("user_mentions")){
                e.setMentions(readMentions(reader));
            }
            else{
                reader.skipValue();
            }
        }
        
        reader.endObject();
        return e;
    }

    private String[] readHashtags(JsonReader reader) throws IOException {
        logger.trace("Reading Hashtags");
        ArrayList<String> hashtags = new ArrayList<String>();
        reader.beginArray();
        
        while(reader.hasNext()){
            String s = readHashtag(reader);
            hashtags.add(s);
        }
        
        reader.endArray();
        
        String[] res = new String[hashtags.size()];
        hashtags.toArray(res);
        return res;
    }

    private String readHashtag(JsonReader reader) throws IOException {
        logger.trace("Reading hashtag.");
        String s = "";
        reader.beginObject();
        
        while(reader.hasNext()){
            String name = reader.nextName();
            if(name.equals("text")){
                s = reader.nextString();
            }
            else{
                reader.skipValue();
            }
        }
        
        reader.endObject();
        return s;
    }

    private TweetUser[] readMentions(JsonReader reader) throws IOException {
        logger.trace("Reading mentionds.");
        ArrayList<TweetUser> ul = new ArrayList<TweetUser>();
        reader.beginArray();
        
        while(reader.hasNext()){
            TweetUser u = readUser(reader);
            ul.add(u);
        }
        
        reader.endArray();
        
        TweetUser[] res = new TweetUser[ul.size()];
        ul.toArray(res);
        return res;
    }

    private class Entity{
        
        private String[] hashtags = {};
        private TweetUser[] mentions = {};

        public String[] getHashtags() {
            return hashtags;
        }

        public void setMentions(TweetUser[] mentions) {
            this.mentions = mentions;
        }

        public void setHashtags(String[] hashtags) {
            this.hashtags = hashtags;
        }

        public TweetUser[] getMentions() {
            return mentions;
        }
        
    }
    
}
