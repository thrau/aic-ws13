package at.ac.tuwien.infosys.twittalyst.analyzer.dump;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.common.util.FileFinder;

/**
 * Defines a directory as a repository which it can find {@link StreamDump} instances from.
 */
public class StreamDumpRepository {

    private static final Logger LOG = LoggerFactory.getLogger(StreamDumpRepository.class);

    private Path directory;

    /**
     * Registers a {@link StreamDumpRepository} in the given directory.
     * 
     * @param directory the path to the directory
     */
    public StreamDumpRepository(String directory) {
        this(Paths.get(directory));
    }

    /**
     * Registers a {@link StreamDumpRepository} in the given directory.
     * 
     * @param directory the path to the directory
     */
    public StreamDumpRepository(Path directory) {
        this.directory = directory;
    }

    /**
     * Returns all {@link StreamDump}s in the repository.
     * 
     * @return a list of {@link StreamDump} objects.
     */
    @SuppressWarnings("unchecked")
    public List<StreamDump> getAll() {
        try {
            return find(directory);
        } catch (IOException e) {
            LOG.error("IO Exception while trying to find StreamDump in {}", directory, e);
            return Collections.EMPTY_LIST;
        }
    }

    /**
     * Searches for a {@link StreamDump} with the given id, which is a 7 character hex value.
     * 
     * @param id the id of the stream dump
     * @return the {@link StreamDump} object with the given id, or null if it was not found.
     */
    public StreamDump getById(String id) {
        try {
            return findById(id, directory);
        } catch (IOException e) {
            LOG.error("IO Exception while trying to find StreamDump in {}", directory, e);
            return null;
        }
    }

    /**
     * Creates a new {@link StreamDump} in the repository. Does not implicitly create files.
     * 
     * @return a new {@link StreamDump}
     */
    public StreamDump newStreamDump() {
        return new StreamDump(directory);
    }

    /**
     * Returns the path to the directory of this repository.
     * 
     * @return a path object pointing to the repository's directory
     */
    public Path getDirectory() {
        return directory;
    }

    /**
     * Collects all files that make up StreamDumps in the given directory.
     * 
     * @param directory the root directory in which to look for
     * @return a list of {@link StreamDump} objects.
     * @throws IOException
     */
    private static List<StreamDump> find(Path directory) throws IOException {
        return find(directory, String.format(StreamDump.GLOB, "*"));
    }

    private static StreamDump findById(String id, Path directory) throws IOException {
        List<StreamDump> find = find(directory, String.format(StreamDump.GLOB, id));

        return (CollectionUtils.isEmpty(find)) ? null : find.get(0);
    }

    private static List<StreamDump> find(Path directory, String pattern) throws IOException {
        List<Path> paths = FileFinder.find(directory, pattern);

        Map<String, StreamDump> dumps = new HashMap<>();

        for (Path path : paths) {
            Matcher matcher = StreamDump.REGEX_PATTERN.matcher(path.getFileName().toString());

            if (matcher.find()) {
                String id = matcher.group(2);

                if (!dumps.containsKey(id)) {
                    dumps.put(id, new StreamDump(directory, id));
                }
            }
        }

        return new ArrayList<>(dumps.values());
    }
}
