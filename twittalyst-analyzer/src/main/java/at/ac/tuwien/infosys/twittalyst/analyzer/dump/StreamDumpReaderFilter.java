package at.ac.tuwien.infosys.twittalyst.analyzer.dump;

import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;

/**
 * A closure that may accept or refuse {@link Tweet}s.
 */
public interface StreamDumpReaderFilter {
    /**
     * Analyzes the given tweet and checks whether it is to be filtered or not.
     * 
     * @param tweet the tweet to analyze.
     * @return true if the tweet passes, false if it is to be filtered out.
     */
    boolean accept(Tweet tweet);
}
