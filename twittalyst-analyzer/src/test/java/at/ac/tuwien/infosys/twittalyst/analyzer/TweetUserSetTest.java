package at.ac.tuwien.infosys.twittalyst.analyzer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetUser;

public class TweetUserSetTest {

    private TweetUserSet set;

    @Before
    public void setUp() {
        set = new TweetUserSet();
    }

    @After
    public void tearDown() {
        set = null;
    }

    @Test
    public void add_behavesCorrectly() throws Exception {
        TweetUser user = new TweetUser();
        user.setId(1);

        set.add(user);

        assertEquals(1, set.size());
        assertTrue(set.contains(new Long(1)));
        assertTrue(set.contains(user));
        assertEquals(new Long(-1), set.iterator().next().getTweets());

        TweetUser userEnriched = new TweetUser();
        userEnriched.setId(1);
        userEnriched.setTweets(42);
        set.add(userEnriched);
        set.add(user);

        TweetUser userNotContained = new TweetUser();
        userNotContained.setId(2);

        assertEquals(1, set.size());
        assertTrue(set.contains(new Long(1)));
        assertTrue(set.contains(user));
        assertTrue(set.contains(userEnriched));
        assertFalse(set.contains(userNotContained));

        assertEquals(new Long(42), set.iterator().next().getTweets());
    }

    @Test
    public void remove_behavesCorrectly() throws Exception {
        TweetUser user = new TweetUser();
        user.setId(1);

        TweetUser userEnriched = new TweetUser();
        userEnriched.setId(1);
        userEnriched.setTweets(42);

        set.add(userEnriched);
        set.add(user);

        set.remove(user);

        assertEquals(0, set.size());
    }

}
