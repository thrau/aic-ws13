package at.ac.tuwien.infosys.twittalyst.analyzer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;
import at.ac.tuwien.infosys.twittalyst.datastore.docdb.AdvertisementDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:ac-analyzer-test.xml",  "classpath:ac-datastore-test.xml" })
public class TopicDestilatorIT {

    @Autowired
    private TopicDestilator td;
    
    @Autowired
    private AdvertisementDAO dao;
    
    @Test
    public void testDestilate() {
        Tweet t = new Tweet();
        t.setText("#believemovie ist mein liebstes Ding.");
        List<String> res = td.destilate(t);
        System.out.println(res.toString());
        Assert.assertTrue("Topic sollte mit Music matchen", res.contains("Music"));
    }
    
    @Test
    public void testDestilate2() {
        Tweet t = new Tweet();
        t.setText("@BBCOne?: ist mein liebstes Ding.");
        List<String> res = td.destilate(t);
        System.out.println(res.toString());
        Assert.assertTrue("Topic sollte mit TVShow matchen", res.contains("TVShow"));
    }
    

}
