package at.ac.tuwien.infosys.twittalyst.analyzer;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDump;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.SimpleStreamDumpReader;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetUser;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.InteractsRelation;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.UserNode;
import at.ac.tuwien.infosys.twittalyst.datastore.graphdb.repositories.UserRepository;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.User;
import at.ac.tuwien.infosys.twittalyst.datastore.rdb.UserDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:ac-analyzer-test.xml",  "classpath:ac-datastore-test.xml"})
public class DumpAnalyzerIT {
    
    @Autowired
    private DumpAnalyzer analyzer;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserDAO userDAO;
    
    @Value("${classpath*:/twittalyst_shortdump.txt}")
    private String nameOfDumpFile;
    
    @Test
    public void testAnalyzeTweetsFromDump_findsDumpFile(){
        URL location = this.getClass().getResource(nameOfDumpFile);
    	File dumpFile = new File(location.getPath());
    	assertTrue(dumpFile.isFile());
    }
    
    @Test
    public void testAnalyzeTweetsFromDump_traverseDumpFile() throws IOException {
        URL location =  this.getClass().getResource(nameOfDumpFile);
    	File dumpFile = new File(location.getPath());
        SimpleStreamDumpReader streamDumpReader = new SimpleStreamDumpReader(new StreamDump(dumpFile, null, null));
    	analyzer.analyzeDump(streamDumpReader);
    }
    
    @Test
    public void testAnalyzeTweetWithMentionedUser() {
        TweetUser user1 = new TweetUser();
        long user1_id = 13222L;
        user1.setId(user1_id);
        user1.setScreenName("user1");
        user1.setFriends(20);
        user1.setFollowers(42);
        user1.setTweets(30L);
        
        TweetUser user2 = new TweetUser();
        long user2_id = 12479922L;
        user2.setId(user2_id);
        user2.setScreenName("user2");
        
        
        Tweet tweet = new Tweet();
        tweet.setAuthor(user1);
        tweet.setHashtags(new String[] {"aic", "testing"});
        tweet.setMentions(new TweetUser[] {user2});
        
        analyzer.processTweet(tweet);
        
        User user = userDAO.getByID(user1_id);
        assertNotNull(user);
        assertEquals("user1", user.getName());
        assertEquals(new Long(42L), user.getFollowerCount());
        
        user = userDAO.getByID(user2_id);
        assertNotNull(user);
        assertEquals("user2", user.getName());
        
        
        UserNode userNode = userRepository.findByUserId(user1_id);
        assertNotNull(userNode);
        Set<InteractsRelation> interactions = userNode.getInteractingUsers();
        assertEquals(1L, interactions.size());
        
        InteractsRelation interaction = userNode.getInteractingUsers().iterator().next();
        assertEquals(1L, interaction.weight.longValue());
        
        assertNotNull("Missing user1 in interaction", interaction.user1);
        assertNotNull("Missing user2 in interaction", interaction.user2);
        assertNotNull("Missing user1's userId in interaction", interaction.user1.getUserId());
        assertNotNull("Missing user2's userId in interaction", interaction.user2.getUserId());
        
        assertEquals("Interaction contains wrong user1 ID", user1.getId(), interaction.user1.getUserId().longValue());
        assertEquals("Interaction contains wrong user2 ID", user2.getId(), interaction.user2.getUserId().longValue());
    }
    
    @Test
    public void testAnalyzeTweetsWithMentionedUsers() {
        TweetUser userA = new TweetUser();
        long userA_id = 123001;
        userA.setId(userA_id);
        userA.setScreenName("userA");
        userA.setFriends(20);
        userA.setFollowers(42);
        userA.setTweets(30L);
        
        TweetUser userB = new TweetUser();
        long userB_id = 123002L;
        userB.setId(userB_id);
        userB.setScreenName("userB");
        userA.setTweets(2L);
        
        TweetUser userC = new TweetUser();
        long userC_id = 123003L;
        userC.setId(userC_id);
        userC.setScreenName("userC");
        
        
        Tweet tweet1 = new Tweet();
        tweet1.setAuthor(userA);
        tweet1.setText("I'm #testing with @userB and @userC #aic");
        tweet1.setHashtags(new String[] {"testing", "aic"});
        tweet1.setMentions(new TweetUser[] {userB, userC});
        
        Tweet tweet2 = new Tweet();
        tweet2.setAuthor(userB);
        tweet2.setText("I can test too @userA");
        tweet2.setMentions(new TweetUser[] {userA});
        
        Tweet tweet3 = new Tweet();
        tweet3.setAuthor(userA);
        tweet3.setText("tweet at @userB");
        tweet3.setMentions(new TweetUser[] {userB});
        
        analyzer.processTweet(tweet1);
        analyzer.processTweet(tweet2);
        analyzer.processTweet(tweet3);
        
        
        assertNotNull(userDAO.getByID(userA_id));
        assertNotNull(userDAO.getByID(userB_id));
        assertNotNull(userDAO.getByID(userC_id));
        
        UserNode userANode = userRepository.findByUserId(userA_id);
        UserNode userBNode = userRepository.findByUserId(userB_id);
        UserNode userCNode = userRepository.findByUserId(userC_id);
        assertNotNull(userANode);
        assertNotNull(userBNode);
        assertNotNull(userCNode);
        
        Set<InteractsRelation> interactionsA = userANode.getInteractingUsers();
        System.out.println(interactionsA);
        assertEquals(2L, interactionsA.size());
        
        Set<InteractsRelation> interactionsB = userBNode.getInteractingUsers();
        System.out.println(interactionsB);
        assertEquals(1L, interactionsB.size());
        
        Set<InteractsRelation> interactionsC = userCNode.getInteractingUsers();
        System.out.println(interactionsC);
        assertEquals(0L, interactionsC.size());
        
        Iterator<InteractsRelation> it = userANode.getInteractingUsers().iterator();
        while(it.hasNext()) {
            InteractsRelation interaction = it.next();
            assertEquals(userANode, interaction.user1);
            
            long user2Id = interaction.user2.getUserId().longValue();
            if (user2Id == userBNode.getUserId())
                assertEquals("Wrong weight", 2L, interaction.weight.longValue());
            else if (user2Id == userCNode.getUserId())
                assertEquals("Wrong weight", 1L, interaction.weight.longValue());
            else 
                fail("Interaction contains unknown user2");
        }
        
        InteractsRelation interaction = userBNode.getInteractingUsers().iterator().next();
        assertEquals(userBNode, interaction.user1);
        assertEquals(userANode.getUserId(), interaction.user2.getUserId());
        assertEquals(1L, interaction.weight.longValue());
    }
    
    @Test
    public void testProcessUserShouldUpdateMetaInformation(){
        TweetUser userA = new TweetUser();
        long userA_id = 1230031;
        userA.setId(userA_id);
        userA.setScreenName("userA1");
        userA.setFriends(20);
        userA.setFollowers(42);
        userA.setTweets(30L);
        
        TweetUser userB = new TweetUser();
        long userB_id = 1230302L;
        userB.setId(userB_id);
        userB.setScreenName("userB1");
    	
        
        Tweet tweet1 = new Tweet();
        tweet1.setAuthor(userA);
        tweet1.setText("I'm #testing with @userB and @userC #aic");
        tweet1.setHashtags(new String[] {"testing", "aic"});
        
        Tweet tweet2 = new Tweet();
        tweet2.setAuthor(userB);
        tweet2.setText("I can test too @userA");
        tweet2.setMentions(new TweetUser[] {userA});
        
        Tweet tweet3 = new Tweet();
        tweet3.setAuthor(userA);
        tweet3.setText("tweet at @userB");
        tweet3.setMentions(new TweetUser[] {userB});
        
        analyzer.processTweet(tweet1);
        analyzer.processTweet(tweet2);
        analyzer.processTweet(tweet3);
          
        assertEquals(new Long(-1),userDAO.getByID(userB_id).getTweetCount());
        assertEquals(new Long(-1),userDAO.getByID(userB_id).getFriendCount());
        assertEquals(new Long(-1),userDAO.getByID(userB_id).getFollowerCount());
        
        userB.setFriends(20);
        userB.setFollowers(42);
        userB.setTweets(30L);
        
        Tweet tweet4 = new Tweet();
        tweet4.setAuthor(userB);
        tweet4.setText("I can test too @userA");
        tweet4.setMentions(new TweetUser[] {userA});
        
        analyzer.processTweet(tweet4);
        
        User dbUser=userDAO.getByID(userB_id);
          
        assertEquals(new Long(30),dbUser.getTweetCount());
        assertEquals(new Long(20),dbUser.getFriendCount());
        assertEquals(new Long(42),dbUser.getFollowerCount()); 
    }
    
    @Test
    public void testProcessTweetUsersShouldSaveRefferedUser(){
        TweetUser userA = new TweetUser();
        long userA_id = 1230015;
        userA.setId(userA_id);
        userA.setScreenName("userA22");
        userA.setFriends(20);
        userA.setFollowers(42);
        userA.setTweets(30L);
        
        TweetUser userB = new TweetUser();
        long userB_id = 1230010L;
        userB.setId(userB_id);
        userB.setScreenName("userB22");
        userA.setTweets(2L);
        
        TweetUser userC = new TweetUser();
        long userC_id = 123013L;
        userC.setId(userC_id);
        userC.setScreenName("userC22");
        
        
        Tweet tweet1 = new Tweet();
        tweet1.setAuthor(userA);
        tweet1.setText("I'm #testing with @userB and @userC #aic");
        tweet1.setHashtags(new String[] {"testing", "aic"});
        tweet1.setMentions(new TweetUser[] {userB, userC});
        tweet1.setRefferedToUser(new TweetUser());
        tweet1.getRefferedToUser().setId(1111);
        
        analyzer.processTweet(tweet1);
        
        User referredUser= userDAO.getByID(1111);
        
        assertNotNull(referredUser);
        assertEquals(referredUser.getID(),1111);
        
        TweetUser userD = new TweetUser();
        long userD_id = 1111;
        userD.setId(userD_id);
        userD.setScreenName("userD22");
        userD.setFriends(20);
        userD.setFollowers(42);
        userD.setTweets(30L);
        
        Tweet tweet2 = new Tweet();
        tweet2.setAuthor(userA);
        tweet2.setText("I'm #testing with @userD and @userC #aic");
        tweet2.setHashtags(new String[] {"testing", "aic"});
        tweet2.setMentions(new TweetUser[] {userD, userC});
        tweet1.setRefferedToUser(new TweetUser());
        tweet1.getRefferedToUser().setId(1111);
        
        analyzer.processTweet(tweet2);
        
        referredUser= userDAO.getByID(1111);
        
        assertNotNull(referredUser);
        assertEquals(new Long(20),referredUser.getFriendCount());
        
    }
    
    
    @After
    public void tearDown() {
        userRepository.deleteAll();
    }

}
