package at.ac.tuwien.infosys.twittalyst.analyzer;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetType;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:ac-analyzer-test.xml",  "classpath:ac-datastore-test.xml"})
public class TweetParserUnitTest {
    
    @Autowired
    private TweetParser parser;
    
    private Tweet tweet = new Tweet();
    private String jsonTweet = "{\"text\":\"Some text\",\"author\":{\"id\":0,\"followers\":-1,\"friends\":-1,\"tweets\":-1,\"verified\":false},\"type\":\"TWEET\",\"mentions\":[{\"id\":0,\"followers\":-1,\"friends\":-1,\"tweets\":-1,\"verified\":false}],\"hashtags\":[\"one\",\"two\"]}";
    private String tweetFromStream = "";    

    @Test
    public void toJSON(){
        makeTweet();
        
        String json = parser.toJSON(tweet);
        Assert.assertEquals(jsonTweet,json);
    }

    private void makeTweet() {
        TweetUser user = new TweetUser();
        String[] hashtags = {"one","two"};
        TweetUser[] mentions = {new TweetUser()};
        
        tweet.setAuthor(user);
        tweet.setHashtags(hashtags);
        tweet.setMentions(mentions);
        tweet.setText("Some text");
        tweet.setType(TweetType.TWEET);
    }
    
    @Test
    public void fromJSON(){
        makeTweet();
        Tweet t = parser.fromJSON(jsonTweet);
        //yeah yeah its not a nice test case...
        Assert.assertEquals(t.toString(), tweet.toString());
    }
    
    @Test
    public void fromTwitterStreamReply() throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/tw.json")));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }
        
        tweetFromStream = stringBuilder.toString();
        
        Tweet t = parser.fromTwitterStream(tweetFromStream);

        Assert.assertEquals(TweetType.REPLY, t.getType());
    }
    
    @Test
    public void fromTwitterStreamTweet() throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/tw2.json")));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }
        
        tweetFromStream = stringBuilder.toString();
        
        Tweet t = parser.fromTwitterStream(tweetFromStream);
        Assert.assertEquals(TweetType.TWEET,t.getType());
    }

}
