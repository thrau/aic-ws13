package at.ac.tuwien.infosys.twittalyst.mine.follow;

/**
 * Provides a list of users to follow.
 */
public interface FollowProvider {

    /**
     * Returns a list of twitter user ids.
     * 
     * @return a list of twitter user ids.
     */
    long[] getUserIds();
}
