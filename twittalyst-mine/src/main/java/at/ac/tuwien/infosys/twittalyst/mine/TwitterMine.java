package at.ac.tuwien.infosys.twittalyst.mine;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.FilterQuery;
import twitter4j.RawStreamListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDumpRepository;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDumpWriter;
import at.ac.tuwien.infosys.twittalyst.common.util.Utils;
import at.ac.tuwien.infosys.twittalyst.mine.follow.FollowProvider;

/**
 * Connects to the {@link TwitterStream} and mines data from the public stream sample filtered by the users returned by
 * a specified {@link FollowProvider}.
 */
public class TwitterMine implements Runnable, Closeable {

    private static final Logger LOG = LoggerFactory.getLogger(TwitterMine.class);

    private TwitterStream stream;
    private ExecutorService executor;

    private RawStreamListener streamListener;
    private FollowProvider followProvider;

    private StreamDumpRepository streamDumpRepository;

    private StreamDumpWriter dumpWriter;

    public TwitterMine(StreamDumpRepository streamDumpRepository, FollowProvider followProvider) {
        this.streamDumpRepository = streamDumpRepository;
        this.followProvider = followProvider;

        streamListener = new StreamHandler();
        stream = TwitterStreamFactory.getSingleton();
        executor = Executors.newCachedThreadPool();
    }

    @Override
    public void run() {
        start();
    }

    /**
     * Starts the twitter by initializing all streams and connecting to the public twitter stream feed.
     */
    public void start() {
        LOG.info("Starting TwitterMine");
        long[] userIds = followProvider.getUserIds();

        dumpWriter = new StreamDumpWriter(streamDumpRepository.newStreamDump());

        saveFollowingList(userIds);
        startFollowing(userIds);
    }

    @Override
    public void close() {
        LOG.info("Shutting down TwitterMine");

        if (stream != null) {
            LOG.debug("Shutting down stream");
            stream.shutdown();
        }

        if (streamListener != null && streamListener instanceof Closeable) {
            try {
                ((Closeable) streamListener).close();
            } catch (IOException e) {
                LOG.error("Error while shutting down stream listener");
            }
        }

        if (executor != null) {
            Utils.shutdownAndAwaitTermination(executor);
        }
    }

    private void startFollowing(long[] users) {
        stream.addListener(streamListener);

        if (!ArrayUtils.isEmpty(users)) {
            FilterQuery query = new FilterQuery(users);
            query.language(new String[] { "en" });

            LOG.info("Listening on public stream filtered by {} users", users.length);
            stream.filter(query);
        } else {
            LOG.info("Listening on random sample of public stream");
            stream.sample();
        }
    }

    private void saveFollowingList(final long[] userIds) {
        LOG.debug("Saving {} users ids that we're following", userIds.length);

        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    dumpWriter.writeFollowList(userIds);
                } catch (IOException e) {
                    LOG.error("Error writing follow list", e);
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private class StreamHandler implements RawStreamListener, Closeable {

        private int bufferSize = 256;
        private int bufferCnt;

        private boolean closed;

        private List<String> buffer = new ArrayList<>(bufferSize);

        @Override
        public void onException(Exception ex) {
            throw new RuntimeException(ex);
        }

        @Override
        public void onMessage(String rawString) {
            if (closed) {
                return;
            }

            if (passes(rawString)) {
                buffer.add(rawString);

                if (++bufferCnt == bufferSize) {
                    LOG.debug("received {} tweets, writing buffer", bufferCnt);
                    writeBuffer(new ArrayList<>(buffer));
                    bufferCnt = 0;
                    buffer = new ArrayList<>(bufferSize);
                }
            }
        }

        boolean passes(String rawString) {
            return rawString.startsWith("{\"created_at");
        }

        void writeBuffer(final List<String> messages) {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        synchronized (dumpWriter) {
                            dumpWriter.write(messages);
                        }
                    } catch (IOException e) {
                        LOG.error("Error writing into dump", e);
                        throw new RuntimeException(e);
                    }
                }
            });
        }

        @Override
        public void close() throws IOException {
            LOG.info("closing stream handler");
            closed = true;

            if (dumpWriter != null) {
                synchronized (dumpWriter) {
                    dumpWriter.write(buffer);

                    bufferCnt = 0;
                    buffer = null;
                }
            }

        }

    }

}
