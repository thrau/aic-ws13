package at.ac.tuwien.infosys.twittalyst.mine.follow;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.common.util.Collections;
import at.ac.tuwien.infosys.twittalyst.common.util.Utils;

/**
 * Task to scrape the top users in the "who-to-follow" list of Twitter.
 * 
 * This generates about 210 users that we can use as a starting point to build our data set.
 */
public class OpinionBootstrap implements FollowProvider {
    private static final Logger LOG = LoggerFactory.getLogger(OpinionBootstrap.class);

    private static final String RESOURCE = "who_to_follow.html";

    @Override
    public long[] getUserIds() {
        Document doc;

        try (InputStream stream = Utils.getResourceAsStream(RESOURCE)) {
            doc = Jsoup.parse(stream, "UTF-8", "https://twitter.com/who_to_follow/interests");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        List<Long> ids = new ArrayList<>();

        Elements elements = doc.select("a.user-static-list-item");

        for (Element element : elements) {
            try {
                long id = Long.parseLong(element.attr("data-user-id"));
                if (id > 0) {
                    ids.add(id);
                }
            } catch (NumberFormatException e) {
                LOG.warn("Could not parse data-user-id", e);
            }
        }

        return Collections.asArray(ids);
    }

}
