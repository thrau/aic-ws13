package at.ac.tuwien.infosys.twittalyst.mine;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.analyzer.TweetUserSet;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.SimpleStreamDumpReader;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDump;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDumpReader;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDumpRepository;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.Tweet;
import at.ac.tuwien.infosys.twittalyst.analyzer.model.TweetUser;
import at.ac.tuwien.infosys.twittalyst.common.util.CountingSet;

/**
 * Converts a stream dump into two csv files containing the nodes and edges respectively, that can be imported into
 * gephi to build a graph.
 * <p>
 * Edges are mentions and are weighted by their re-occurrence.
 */
public class GephiCsvConverter {

    public static void main(String[] args) throws IOException {

        final StreamDumpRepository repo = Main.getBean("streamDumpRepository");

        for (final StreamDump dump : repo.getAll()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    GephiCsvConverter gephiConverter = new GephiCsvConverter(dump);
                    try {
                        gephiConverter.doConvert(repo.getDirectory());
                    } catch (IOException e) {
                        LOG.error("IO error while converting dump {}", dump);
                    }
                }
            }).start();
        }

    }

    private static final Logger LOG = LoggerFactory.getLogger(GephiCsvConverter.class);
    private static final Charset UTF8 = Charset.forName("UTF-8");

    private StreamDump dump;

    public GephiCsvConverter(StreamDump dump) {
        this.dump = dump;
    }

    /**
     * Begins the conversion process by reading the stream dump and then writing two csv files.
     * 
     * @param directory the directory in which to write the csv files.
     * @throws IOException
     */
    public void doConvert(Path directory) throws IOException {
        final TweetUserSet nodes = new TweetUserSet(1024);
        final CountingSet<Edge> edges = new CountingSet<>(1024);

        LOG.debug("analyzing stream dump {}", dump.getId());
        try (StreamDumpReader reader = new SimpleStreamDumpReader(dump)) {
            Tweet t;
            while ((t = reader.readTweet()) != null) {
                nodes.add(t.getAuthor());

                for (TweetUser mention : t.getMentions()) {
                    nodes.add(mention);
                    edges.add(new Edge(t.getAuthor(), mention));
                }
            }
        }

        doWriteNodes(nodes, directory);
        doWriteEdges(edges, directory);
    }

    private void doWriteEdges(final CountingSet<Edge> edges, final Path directory) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                LOG.debug("writing {} edges ...", edges.size());
                try (BufferedWriter edgeFileWriter = newEdgeFileWriter(directory)) {
                    edgeFileWriter.append("Source,Target,Type,Id,Label,Weight\n");
                    long i = 1;
                    for (Edge edge : edges) {
                        edgeFileWriter.append(String.format("%d,%d,Directed,%d,,%d\n", edge.getSource(),
                                edge.getTarget(), i++, edges.count(edge)));
                    }
                } catch (IOException e) {
                    LOG.error("IO error while writing to edge file {}", getEdgeFilePath(directory), e);
                }

            }
        }).start();
    }

    private void doWriteNodes(final TweetUserSet nodes, final Path directory) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                LOG.debug("writing {} nodes ...", nodes.size());
                try (BufferedWriter nodeFileWriter = newNodeFileWriter(directory)) {
                    nodeFileWriter.append("Id,Label\n");
                    for (TweetUser node : nodes) {
                        nodeFileWriter.append(String.format("%d,%s", node.getId(), node.getScreenName()) + "\n");
                    }
                } catch (IOException e) {
                    LOG.error("IO error while writing to node file {}", getNodeFilePath(directory), e);
                }
            }
        }).start();
    }

    private BufferedWriter newNodeFileWriter(Path directory) throws IOException {
        return Files.newBufferedWriter(getNodeFilePath(directory), UTF8);
    }

    private BufferedWriter newEdgeFileWriter(Path directory) throws IOException {
        return Files.newBufferedWriter(getEdgeFilePath(directory), UTF8);
    }

    private Path getNodeFilePath(Path directory) {
        return directory.resolve("gephi.nodes." + dump.getId() + ".csv");
    }

    private Path getEdgeFilePath(Path directory) {
        return directory.resolve("gephi.edges." + dump.getId() + ".csv");
    }

    /**
     * Value object containing the source and target of a twitter mention.
     */
    private static class Edge {
        private final long source;
        private final long target;

        public Edge(TweetUser source, TweetUser target) {
            this(source.getId(), target.getId());
        }

        public Edge(long source, long target) {
            this.source = source;
            this.target = target;
        }

        public long getSource() {
            return source;
        }

        public long getTarget() {
            return target;
        }

        @Override
        public int hashCode() {
            int result = 1;
            result = 37 * result + (int) (source ^ (source >>> 32));
            result = 31 * result + (int) (target ^ (target >>> 32));
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj instanceof Edge) {
                return equals((Edge) obj);
            }
            return super.equals(obj);
        }

        public boolean equals(Edge edge) {
            return target == edge.getTarget() && source == edge.getSource();
        }

    }

}
