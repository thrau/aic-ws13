package at.ac.tuwien.infosys.twittalyst.mine.follow;

public class EmptyFollowProvider implements FollowProvider {

    private static final long[] EMPTY_ARRAY = new long[] {};

    @Override
    public long[] getUserIds() {
        return EMPTY_ARRAY;
    }

}
