package at.ac.tuwien.infosys.twittalyst.mine.follow;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.twittalyst.analyzer.TweetUserSet;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDump;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDumpAnalyzer;
import at.ac.tuwien.infosys.twittalyst.analyzer.dump.StreamDumpRepository;

/**
 * Provides user IDs to follow based on previous stream dumps using the {@link StreamDumpAnalyzer#whoToFollow(int)}
 * routine.
 */
public class HistoricFollowProvider implements FollowProvider {

    private static final Logger LOG = LoggerFactory.getLogger(HistoricFollowProvider.class);

    private static final int USER_LIMIT = 5000;
    private static final int ITERATIONS = 4;

    private StreamDumpRepository streamDumpRepository;

    /**
     * Creates a new FollowProvider with the given StreamDumpRepository.
     * 
     * @param streamDumpRepository the repository to look for stream dumps in.
     */
    public HistoricFollowProvider(StreamDumpRepository streamDumpRepository) {
        this.streamDumpRepository = streamDumpRepository;
    }

    @Override
    public long[] getUserIds() {
        TweetUserSet follow = new TweetUserSet();

        for (StreamDump dump : streamDumpRepository.getAll()) {
            LOG.debug("analyzing {}", dump);
            StreamDumpAnalyzer analyzer = new StreamDumpAnalyzer(dump);
            analyzer.whoToFollow(ITERATIONS, USER_LIMIT, follow);

            if (follow.size() > USER_LIMIT) {
                return Arrays.copyOf(follow.getUserIdArray(), USER_LIMIT);
            }
        }

        return follow.getUserIdArray();

    }

}
