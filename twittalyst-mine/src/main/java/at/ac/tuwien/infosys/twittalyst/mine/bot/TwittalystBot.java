package at.ac.tuwien.infosys.twittalyst.mine.bot;

import java.io.Closeable;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.RateLimitStatus;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.UserMentionEntity;
import twitter4j.UserStreamAdapter;
import at.ac.tuwien.infosys.twittalyst.common.util.Collections;

/**
 * Stupid and ugly hack that turned out to be illegal :P
 */
public class TwittalystBot implements Runnable, Closeable {

    public static void main(String[] args) {
        TwittalystBot twittalystBot = new TwittalystBot();

        new Thread(twittalystBot).start();
    }

    private static final Logger LOG = LoggerFactory.getLogger(TwittalystBot.class);

    private static final String[] BADWORDS = { "fast", "gain", "followers", "follow", "retweet", "everyone",
        "TityFollowTrain", "nigga", "ComedyOrTruth", "❤" };
    private Twitter twitter;
    private TwitterStream stream;

    private BotStreamListener streamListener;

    private TwittalystBotInfo botInfo;

    private FollowBot followBot;

    public TwittalystBot() {
        this(TwitterFactory.getSingleton(), TwitterStreamFactory.getSingleton());
    }

    public TwittalystBot(Twitter twitter, TwitterStream stream) {
        this.twitter = twitter;
        this.stream = stream;
        this.streamListener = new BotStreamListener();
        this.botInfo = new TwittalystBotInfo();
        this.stream.addListener(this.streamListener);
        this.followBot = new FollowBot();
    }

    @Override
    public void run() {
        try {
            bootstrap();
        } catch (TwitterException e) {
            throw new RuntimeException("Error while bootstrapping twittalyst bot", e);
        }
        stream.user();
        followBot.start();
    }

    @Override
    public void close() {
        LOG.info("Shutting down TwittalystBot");

        if (followBot != null && followBot.isRunning()) {
            followBot.stop();
        }
        if (stream != null) {
            stream.cleanUp();
        }
    }

    private void bootstrap() throws TwitterException {
        botInfo.setRateLimitStatus(twitter.getRateLimitStatus());
        botInfo.setScreenName(twitter.getScreenName());
        LOG.debug("current rate limits {}", botInfo.rateLimitMap);
    }

    private boolean shouldIgnore(Status status) {
        String text = status.getText().toLowerCase().trim();

        for (String badword : BADWORDS) {
            if (text.contains(badword)) {
                return true;
            }
        }

        return false;
    }

    private class BotStreamListener extends UserStreamAdapter {
        @Override
        public void onStatus(Status status) {
            LOG.trace("received status {}", status);

            if (shouldIgnore(status)) {
                LOG.debug("bad status");
                return;
            }

            if (!botInfo.hasFriend(status.getUser().getId())) {
                LOG.debug("Status by unfollowed user {}", status.getUser().getScreenName());
                followBot.addCandidate(status.getUser().getId());
            }

            UserMentionEntity[] entities = status.getUserMentionEntities();
            if (!ArrayUtils.isEmpty(entities)) {
                for (UserMentionEntity entity : entities) {
                    if (!botInfo.hasFriend(entity.getId())) {
                        LOG.debug("{} mentions {}", status.getUser().getScreenName(), entity.getScreenName());
                        followBot.addCandidate(entity.getId());
                    }
                }
            }
        }

        @Override
        public void onFollow(User source, User followedUser) {
            LOG.info("{} is now following {}", source.getScreenName(), followedUser.getScreenName());
        }

        @Override
        public void onStallWarning(StallWarning warning) {
            LOG.warn("Received a stall warning {}", warning.getMessage());
        }

        @Override
        public void onFriendList(long[] friendIds) {
            botInfo.setFriends(friendIds);
        }
    }

    private class FollowBot {
        private BlockingQueue<Long> candidates;

        private Timer timer;
        private boolean running = false;
        private int preiod = 10 * 1000;
        private int delay = 30 * 1000;

        public FollowBot() {
            candidates = new LinkedBlockingQueue<>();
            timer = new Timer("FollowBotTimer");
        }

        public void start() {
            running = true;
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Long id = candidates.poll();
                    if (id != null) {
                        try {
                            User user = twitter.createFriendship(id);
                            if (user != null) {
                                botInfo.addFriend(id);
                                LOG.debug("Created friendship with {}", user.getScreenName());
                            }
                        } catch (TwitterException e) {
                            LOG.error("Error creating friendship with {}", id, e);
                        }
                    } else {
                        LOG.warn("Candidate list is empty, don't know who to follow!");
                    }
                }
            }, delay, preiod);
        }

        public void stop() {
            running = false;
            timer.cancel();
        }

        public boolean isRunning() {
            return running;
        }

        public void addCandidate(Long id) {
            if (candidates.size() < 120) {
                try {
                    if (!candidates.contains(id)) {
                        candidates.put(id);
                    }
                } catch (InterruptedException e) {
                    LOG.warn("FollowBot interrupted while trying to add candidate", e);
                }
            }
        }

    }

    public class TwittalystBotInfo {

        private String screenName;
        private Map<String, RateLimitStatus> rateLimitMap;
        private Set<Long> friends;

        public synchronized String getScreenName() {
            return screenName;
        }

        public synchronized void setScreenName(String screenName) {
            this.screenName = screenName;
        }

        public synchronized Set<Long> getFriends() {
            return friends;
        }

        public synchronized void addFriend(long id) {
            this.friends.add(id);
        }

        public synchronized boolean hasFriend(long id) {
            return friends.contains(id);
        }

        public synchronized void setFriends(long[] friends) {
            setFriends(Collections.asSet(friends));
        }

        public synchronized void setFriends(Set<Long> friends) {
            this.friends = friends;
        }

        public synchronized Map<String, RateLimitStatus> getRateLimitStatus() {
            return rateLimitMap;
        }

        public synchronized void updateRateLimitStatus(String endpoint, RateLimitStatus rateLimitStatus) {
            rateLimitMap.put(endpoint, rateLimitStatus);
        }

        public synchronized RateLimitStatus getRateLimitStatus(String endpoint) {
            return rateLimitMap.get(endpoint);
        }

        public synchronized void setRateLimitStatus(Map<String, RateLimitStatus> rateLimitStatus) {
            this.rateLimitMap = rateLimitStatus;
        }

    }

}
