package at.ac.tuwien.infosys.twittalyst.mine;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Main application runner class for running the twitter mine.
 */
public class Main {

    private static final String[] SPRING_CONTEXT = { "ac-mine.xml" };
    private static ApplicationContext APPLICATION_CONTEXT = new ClassPathXmlApplicationContext(SPRING_CONTEXT);

    public static void main(String[] args) throws InterruptedException {
        final TwitterMine mine = getBean("twitterMine");

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                mine.close();
            }
        });

        mine.start();
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(String name) {
        return (T) APPLICATION_CONTEXT.getBean(name);
    }

}
