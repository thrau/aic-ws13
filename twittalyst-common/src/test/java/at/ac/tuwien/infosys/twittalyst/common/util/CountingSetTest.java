package at.ac.tuwien.infosys.twittalyst.common.util;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.ac.tuwien.infosys.twittalyst.common.util.CountingSet;

public class CountingSetTest {

    private CountingSet<String> set;

    @Before
    public void setUp() {
        set = new CountingSet<>();
    }

    @After
    public void tearDown() {
        set = null;
    }

    @Test
    public void add_increasesCount() throws Exception {
        assertEquals(0, set.count("foo"));
        assertEquals(0, set.count("bar"));
        set.add("foo");
        assertEquals(1, set.count("foo"));
        assertEquals(0, set.count("bar"));
        set.add("foo");
        assertEquals(2, set.count("foo"));
        assertEquals(0, set.count("bar"));
        set.add("bar");
        assertEquals(2, set.count("foo"));
        assertEquals(1, set.count("bar"));
    }

    @Test
    public void remove_resetsCount() throws Exception {
        assertEquals(0, set.count("foo"));
        set.add("foo");
        set.add("foo");
        set.remove("foo");
        assertEquals(0, set.count("foo"));
    }

    @Test
    public void retainAll_works() throws Exception {
        set.add("foo");
        set.add("foo");
        set.add("foo");
        set.add("bar");
        set.add("bar");
        set.add("ed");

        set.retainAll(Arrays.asList(new String[] { "ed", "bar" }));

        assertEquals(0, set.count("foo"));
        assertEquals(2, set.count("bar"));
        assertEquals(1, set.count("ed"));
    }

    @Test
    public void removeAll_works() throws Exception {
        set.add("foo");
        set.add("foo");
        set.add("foo");
        set.add("bar");
        set.add("bar");
        set.add("ed");

        set.removeAll(Arrays.asList(new String[] { "ed", "bar" }));

        assertEquals(3, set.count("foo"));
        assertEquals(0, set.count("bar"));
        assertEquals(0, set.count("ed"));
    }

}
