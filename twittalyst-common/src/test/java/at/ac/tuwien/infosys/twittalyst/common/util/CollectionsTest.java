package at.ac.tuwien.infosys.twittalyst.common.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class CollectionsTest {

    @Test
    public void head_returnsCorrectEntries() throws Exception {
        List<String> list = new ArrayList<>();

        list.add("foo");
        list.add("bar");
        list.add("baz");
        list.add("ed");

        List<String> first = Collections.head(list, 2);

        assertEquals(2, first.size());
        assertEquals("foo", first.get(0));
        assertEquals("bar", first.get(1));
    }

    @Test
    public void head_onShorterList_returnsCorrectEntries() throws Exception {
        List<String> list = new ArrayList<>();

        list.add("foo");
        list.add("bar");

        List<String> first = Collections.head(list, 3);

        assertEquals(2, first.size());
        assertEquals("foo", first.get(0));
        assertEquals("bar", first.get(1));
    }


    @Test
    public void map_works() throws Exception {
        List<String> list = Arrays.asList("foo", "b", "ar");

        List<Integer> sizes = Collections.map(list, new Callback<String, Integer>() {
            @Override
            public Integer call(String object) {
                return object.length();
            }
        });

        assertEquals(3, sizes.size());
        assertEquals(new Integer(3), sizes.get(0));
        assertEquals(new Integer(1), sizes.get(1));
        assertEquals(new Integer(2), sizes.get(2));
    }

}
