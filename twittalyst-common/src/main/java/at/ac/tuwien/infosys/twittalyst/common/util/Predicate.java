package at.ac.tuwien.infosys.twittalyst.common.util;

/**
 * A predicate that evaluates a given object to true or false.
 * 
 * @param <T>
 */
public interface Predicate<T> {

    /**
     * Evalutes the given element.
     * 
     * @param element the element to evaluate
     * @return the evaluation result
     */
    boolean evaluate(T element);
}
