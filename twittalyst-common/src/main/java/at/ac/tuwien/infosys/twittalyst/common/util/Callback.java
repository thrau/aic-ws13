package at.ac.tuwien.infosys.twittalyst.common.util;

/**
 * A callback interface.
 */
public interface Callback<E, R> {

    /**
     * Execute the callback on the given object.
     * 
     * @param object the object of the callback
     * @return the result of the callback
     */
    R call(E object);
}
