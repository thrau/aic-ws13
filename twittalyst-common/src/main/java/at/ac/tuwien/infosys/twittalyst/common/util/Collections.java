package at.ac.tuwien.infosys.twittalyst.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.util.StringUtils;

/**
 * Custom collection utilities.
 */
public final class Collections {

    private Collections() {

    }

    /**
     * Return a new collection of all elements of the given collection that satisfy the given predicate.
     * 
     * @param collection the collection to filter
     * @param predicate the filter predicate
     * @param <T> the type of the elements in the collection
     * @return the elements in a new collection (an {@link ArrayList}).
     */
    public static <T> Collection<T> select(Collection<T> collection, Predicate<T> predicate) {
        ArrayList<T> output = new ArrayList<>(collection.size());

        for (T next : collection) {
            if (predicate.evaluate(next)) {
                output.add(next);
            }
        }

        return output;
    }

    /**
     * Return a new map of all elements of the given map that satisfy the given predicate. The predicate is applied to
     * each value in the map.
     * 
     * @param collection the map to filter
     * @param predicate the filter predicate
     * @param <K> the type of the keys in the map
     * @param <V> the type of the elements in the map
     * @return the elements in a new map (a {@link HashMap}).
     */
    public static <K, V> Map<K, V> select(Map<K, V> collection, Predicate<V> predicate) {
        Map<K, V> output = new HashMap<>(collection.size());

        for (Entry<K, V> entry : collection.entrySet()) {
            if (predicate.evaluate(entry.getValue())) {
                output.put(entry.getKey(), entry.getValue());
            }
        }

        return output;
    }

    /**
     * Maps the given callback to every element in the collection.
     * 
     * @param collection the subject collection
     * @param callback the callback function to execute
     * @param <E> the type of the elements in the collection
     * @param <R> the return type of the callback
     * @return the mapped values
     */
    public static <E, R> List<R> map(Collection<E> collection, Callback<E, R> callback) {
        List<R> list = new ArrayList<>(collection.size());

        for (E element : collection) {
            list.add(callback.call(element));
        }

        return list;
    }

    /**
     * Counts the number of elements that match the given predicate.
     * 
     * @param collection the collection to count elements from
     * @param predicate the filter predicate
     * @param <T> the type of the elements in the collection
     * @return the amount of elements that satisfy the given predicate.
     */
    public static <T> int countMatches(Collection<T> collection, Predicate<T> predicate) {
        int count = 0;

        for (Iterator<T> it = collection.iterator(); it.hasNext();) {
            if (predicate.evaluate(it.next())) {
                count++;
            }
        }

        return count;
    }

    /**
     * Counts the number of elements that match the given predicate.The predicate is applied to each value in the map.
     * 
     * @param collection the collection to count elements from
     * @param predicate the filter predicate
     * @param <K> the type of the keys in the map
     * @param <V> the type of the elements in the map
     * @return the amount of elements that satisfy the given predicate.
     */
    public static <K, V> int countMatches(Map<K, V> collection, Predicate<V> predicate) {
        int count = 0;

        for (Entry<K, V> entry : collection.entrySet()) {
            if (predicate.evaluate(entry.getValue())) {
                count++;
            }
        }

        return count;
    }

    /**
     * Creates a map from the given list, using whatever the given callback returns as key. Naturally values will be
     * overwritten, if the callback returns the same value multiple times.
     * 
     * @param list the list to build the map from
     * @param keyCallback the callback used for creating the keys
     * @param <K> the key type
     * @param <V> the value type
     * @return a map!
     */
    public static <K, V> Map<K, V> toMap(List<V> list, Callback<V, K> keyCallback) {
        HashMap<K, V> map = new HashMap<>();

        for (V v : list) {
            map.put(keyCallback.call(v), v);
        }

        return map;
    }

    /**
     * Creates an object set of the given array.
     * 
     * @param arr the array
     * @return a new set containing all distinct elements in the array.
     */
    public static Set<Long> asSet(long[] arr) {
        HashSet<Long> set = new HashSet<>(arr.length);

        for (long e : arr) {
            set.add(e);
        }

        return set;
    }

    /**
     * Returns the given list of objects as an array of primitives.
     * 
     * @param list the list to convert
     * @return the converted array
     */
    public static long[] asArray(List<Long> list) {
        long[] ret = new long[list.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = list.get(i);
        }
        return ret;
    }

    /**
     * Returns the first {@code length} elements in the given collection in a new {@link ArrayList}. If the specified
     * length is larger than the size of the collection, the entire collection is returned.
     * 
     * @param list the collection to pull elements from
     * @param length the amount of elements to pull from
     * @param <T> the type of elements in the collection
     * @return a new collection that contains {@code length} elements
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> head(Collection<T> list, int length) {
        if (length > list.size()) {
            return new ArrayList<>(list);
        }
        return (List<T>) Arrays.asList(Arrays.copyOfRange(list.toArray(), 0, length));
    }

    /**
     * Reverses the effect of {@link Arrays#toString(long[])}.
     * 
     * @param str the serialized array
     * @return the unserialized array
     */
    public static long[] parseLongArray(String str) {
        str = StringUtils.trimLeadingCharacter(str, '[');
        str = StringUtils.trimTrailingCharacter(str, ']');

        String[] token = StringUtils.tokenizeToStringArray(str, ",");
        long[] result = new long[token.length];

        for (int i = 0; i < token.length; i++) {
            result[i] = Long.parseLong(token[i]);
        }

        return result;
    }
}
