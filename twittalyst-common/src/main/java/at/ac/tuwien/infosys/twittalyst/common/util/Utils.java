package at.ac.tuwien.infosys.twittalyst.common.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A mixed set of utility functions.
 */
public final class Utils {
    private static final Logger LOG = LoggerFactory.getLogger(Utils.class);

    private static final int PRIME = 31;

    private static final Random RANDOM = new Random(System.currentTimeMillis() * PRIME);

    private Utils() {

    }

    /**
     * Returns a new {@link InputStream} for the given resource.
     * 
     * @param name the resource name
     * @return a new input stream for the resource
     */
    public static InputStream getResourceAsStream(String name) {
        return getResourceAsStream(Utils.class.getClassLoader(), name);
    }

    /**
     * Returns a new {@link InputStream} for the given resource found in the given class loader.
     * 
     * @param classLoader the class loader to get the resource from.
     * @param name the resource name
     * @return a new input stream for the resource
     */
    public static InputStream getResourceAsStream(ClassLoader classLoader, String name) {
        return classLoader.getResourceAsStream(name);
    }

    /**
     * Reads the entire content of the given file into a String.
     * 
     * @param file the file to read
     * @return the content of the file, or an empty string if an IO error occurred.
     */
    public static String readFile(File file) {
        try {
            return new Scanner(file).useDelimiter("\\Z").next();
        } catch (FileNotFoundException e) {
            LOG.warn("Could not find file {}, returning empty result", file.getPath(), e);
            return "";
        }
    }

    /**
     * Checks the given string whether it contains any of the words in the given array.
     * 
     * @param string the string to analyze
     * @param words the words to check for
     * @return true if the string contains any of the words.
     */
    public static boolean containsAny(String string, String[] words) {
        for (String word : words) {
            if (string.contains(word)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns a fragment of a random SHA-1 Hash.
     * 
     * @param len length of the fragment
     * @return a fragment of a random SHA-1 Hash
     */
    public static String randomId(int len) {
        String sha1 = DigestUtils.sha1Hex(String.valueOf(RANDOM.nextLong() * PRIME * System.currentTimeMillis()));

        if (len >= sha1.length()) {
            return sha1;
        } else {
            return sha1.substring(0, len);
        }
    }

    /**
     * Copied from the java {@link ExecutorService} tutorial. Safe Shutdown for an pool.
     * 
     * @param pool ExecutorService to shut down
     */
    public static void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(1, TimeUnit.MINUTES)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being canceled
                if (!pool.awaitTermination(1, TimeUnit.MINUTES)) {
                    System.out.println("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }
}
