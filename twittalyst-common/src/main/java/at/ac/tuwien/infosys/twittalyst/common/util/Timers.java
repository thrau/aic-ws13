package at.ac.tuwien.infosys.twittalyst.common.util;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Timers {

    private static final Logger LOGGER = LoggerFactory.getLogger(Timers.class);
    private static final int DEFAULT_STRLEN = 128;

    public static final LoggingState INFO = new LoggingState() {
        @Override
        public void log(String msg) {
            LOGGER.info(msg);
        }
    };
    public static final LoggingState DEBUG = new LoggingState() {
        @Override
        public void log(String msg) {
            LOGGER.debug(msg);
        }
    };
    public static final LoggingState TRACE = new LoggingState() {
        @Override
        public void log(String msg) {
            LOGGER.trace(msg);
        }
    };
    public static final LoggingState OFF = new LoggingState() {
        @Override
        public void log(String msg) {
            // pass
        }
    };

    private static LoggingState LOG;

    private static Map<String, Timer> timers = new LinkedHashMap<>();
    
    private static Timer TOTAL = new Timer("Total VM runtime");

    static {
        setLoggingState(DEBUG);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                Timers.logReport();
            }
        });
    }

    public static void setLoggingState(LoggingState state) {
        if (state == null) {
            LOG = OFF;
        } else {
            LOG = state;
        }
    }

    public static Timer start(String name) {
        return timers.put(name, new Timer(name));
    }

    public static Timer stop(String name) {
        Timer timer = timers.get(name);

        if (timer != null) {
            timer.stop();
        }

        return timer;
    }

    public static Timer get(String name) {
        return timers.get(name);
    }

    public static long elapsed(String name) {
        Timer timer = get(name);
        return (timer == null) ? 0 : timer.elapsed();
    }

    public static long now() {
        return System.currentTimeMillis();
    }

    public static StringBuilder report() {
        StringBuilder builder = new StringBuilder(DEFAULT_STRLEN);

        builder.append(StringUtils.center(" Timers ", 63, '=') + "\n");
        for (Timer t : timers.values()) {
            buildReport(t, builder);
            builder.append("\n");
        }
        builder.append(StringUtils.repeat('-', 63) + "\n");
        buildReport(TOTAL, builder);
        builder.append("\n");
        builder.append(StringUtils.repeat('=', 63) + "\n");

        return builder;
    }

    public static void printReport() {
        System.out.println(report().toString());
    }

    public static void logReport() {
        String[] split = StringUtils.split(report().toString(), '\n');
        synchronized (LOG) {
            for (String string : split) {
                LOG.log(string);
            }
        }
    }

    private static void buildReport(Timer timer, StringBuilder builder) {
        timer.toString(builder);
    }

    public static class Timer {
        private String name;
        private long started;
        private long stopped = -1;

        public Timer(String name) {
            this.name = name;
            this.started = now();
        }

        public long stop() {
            if (stopped < 0) {
                stopped = since(started);
            }
            return stopped;
        }

        public long elapsed() {
            if (stopped >= 0) {
                return stopped;
            } else {
                return now() - started;
            }
        }

        private long since(long since) {
            return now() - since;
        }

        public boolean isRunning() {
            return stopped < 0;
        }

        public String getName() {
            return name;
        }

        public long getStarted() {
            return started;
        }

        public long getStopped() {
            return stopped;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder(DEFAULT_STRLEN);
            toString(builder);
            return builder.toString();
        }

        public void toString(StringBuilder builder) {
            builder.append(StringUtils.rightPad(this.getName(), 30));
            builder.append(StringUtils.leftPad(String.valueOf(this.elapsed()), 20));
            builder.append(" ms ");
            if (this.isRunning()) {
                builder.append("[running]");
            } else {
                builder.append("[stopped]");
            }
        }
    }

    public static interface LoggingState {
        public void log(String msg);
    }

}
