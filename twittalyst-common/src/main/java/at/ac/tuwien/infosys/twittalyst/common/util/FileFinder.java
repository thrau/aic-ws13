package at.ac.tuwien.infosys.twittalyst.common.util;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Recursively finds files in a directory based on the given glob pattern.
 */
public class FileFinder {

    private PathMatcher matcher;
    private Path root;
    private int maxDepth;

    /**
     * Creates a new {@link FileFinder} that recursively searches for the given glob pattern in the given root
     * directory.
     * 
     * @param root the directory from which to start searching from
     * @param pattern a glob pattern
     */
    public FileFinder(Path root, String pattern) {
        this(root, pattern, Integer.MAX_VALUE);
    }

    /**
     * Creates a new {@link FileFinder} that searches for the given glob pattern in the given root directory.
     * 
     * @param root the directory from which to start searching from
     * @param pattern a glob pattern
     * @param maxDepth the maximum tree traversal depth
     */
    public FileFinder(Path root, String pattern, int maxDepth) {
        this.root = root;
        this.matcher = FileSystems.getDefault().getPathMatcher("glob:" + pattern);
        this.maxDepth = maxDepth;
    }

    /**
     * Returns the {@link Path} of all files found by this {@code FileFinder}.
     * 
     * @return a list of {@link Path}s of the files that were found.
     * @throws IOException if an IO Error ocurs while walking the file tree.
     */
    public List<Path> find() throws IOException {
        final List<Path> finds = new ArrayList<>();

        FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (matcher.matches(file.getFileName())) {
                    finds.add(file);
                }
                return super.visitFile(file, attrs);
            }
        };

        Files.walkFileTree(root, EnumSet.noneOf(FileVisitOption.class), getDepth(), fv);

        return finds;
    }

    private int getDepth() {
        return (maxDepth > 0) ? maxDepth : Integer.MAX_VALUE;
    }

    /**
     * Equivalent to <code>new FileFinder(root, pattern).find();</code>.
     * 
     * @param root the directory from which to start searching from
     * @param pattern a glob pattern
     * @return a list of {@link Path}s of the files that were found.
     * @throws IOException if an IO Error ocurs while walking the file tree.
     */
    public static final List<Path> find(Path root, String pattern) throws IOException {
        return new FileFinder(root, pattern).find();
    }

    /**
     * Equivalent to <code>new FileFinder(root, pattern, maxDepth).find();</code>.
     * 
     * @param root the directory from which to start searching from
     * @param pattern a glob pattern
     * @param maxDepth the maximum tree traversal depth
     * @return a list of {@link Path}s of the files that were found.
     * @throws IOException if an IO Error ocurs while walking the file tree.
     */
    public static final List<Path> find(Path root, String pattern, int maxDepth) throws IOException {
        return new FileFinder(root, pattern, maxDepth).find();
    }
}
