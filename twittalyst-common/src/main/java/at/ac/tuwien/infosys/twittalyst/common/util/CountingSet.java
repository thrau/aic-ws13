package at.ac.tuwien.infosys.twittalyst.common.util;

import java.io.Serializable;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Acts as a Set, however it counts the amount of times {@link #add(Object)} is called on the given object.
 * 
 * @param <T>
 */
public class CountingSet<T> extends AbstractSet<T> implements Set<T>, Serializable {

    private static final long serialVersionUID = -1569457091919581243L;

    private static final Long ONE = new Long(1);

    private Map<T, Long> map;

    public CountingSet() {
        map = new HashMap<>();
    }

    public CountingSet(int initialCapacity) {
        map = new HashMap<>(initialCapacity);
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    @Override
    public Iterator<T> iterator() {
        return map.keySet().iterator();
    }

    @Override
    public Object[] toArray() {
        return map.keySet().toArray();
    }

    @Override
    public <U> U[] toArray(U[] a) {
        return map.keySet().toArray(a);
    }

    @Override
    public boolean add(T e) {
        Long cnt = map.get(e);
        return null != map.put(e, (cnt == null) ? ONE : new Long(cnt + 1));
    }

    @Override
    public boolean remove(Object o) {
        return map.remove(o) != null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return map.keySet().containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T t : c) {
            add(t);
        }

        return true;
    }

    @Override
    public void clear() {
        map.clear();
    }

    /**
     * Returns a new list of entries sorted by the amount of times they were added in descending order. The key of each
     * entry is the object itself, and the value of the entry is the amount of times the entry was added to the set.
     * 
     * @return a new ordered list.
     */
    public List<Entry<T, Long>> getSortedEntries() {
        List<Entry<T, Long>> set = new ArrayList<>(map.entrySet());

        Collections.sort(set, descending);

        return set;
    }

    /**
     * Returns a new list of entries sorted by the given {@link Comparator}.
     * 
     * @param comparator the comparator to sort by
     * @return a new ordered list.
     */
    public List<Entry<T, Long>> getSortedEntries(Comparator<Entry<T, Long>> comparator) {
        List<Entry<T, Long>> set = new ArrayList<>(map.entrySet());

        Collections.sort(set, comparator);

        return set;
    }

    public Set<Entry<T, Long>> getEntrySet() {
        return map.entrySet();
    }

    /**
     * Returns the amount of times the given element was added to the set.
     * 
     * @param e the element
     * @return the count
     */
    public long count(T e) {
        Long cnt = map.get(e);
        return (cnt != null) ? cnt : 0;
    }

    private final Comparator<Entry<T, Long>> descending = new Comparator<Entry<T, Long>>() {

        @Override
        public int compare(Entry<T, Long> o1, Entry<T, Long> o2) {
            return Long.compare(o2.getValue(), o1.getValue());
        }

    };

}
